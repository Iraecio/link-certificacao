import {NgModule} from '@angular/core';
import {NgxAuthRoutingModule} from './auth-routing.module';
import {NbAuthModule} from '@nebular/auth';
import {NgxLoginComponent} from './login/login.component';
import {NgxLogoutComponent} from './logout/logout.component';
import {SharedModule} from '../@shared/shared.module';
import {ThemeModule} from '../@theme/theme.module';

@NgModule({
    imports: [
        ThemeModule,
        SharedModule,
        NgxAuthRoutingModule,
        NbAuthModule
    ],
    declarations: [
        NgxLoginComponent,
        NgxLogoutComponent,
    ],
})
export class NgxAuthModule {
}
