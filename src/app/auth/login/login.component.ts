import {ChangeDetectorRef, Component, Inject} from '@angular/core';
import {NB_AUTH_OPTIONS, NbAuthResult, NbAuthService, NbLoginComponent} from '@nebular/auth';
import {getDeepFromObject} from '@nebular/auth/helpers';
import {Router} from '@angular/router';
import {ApiService} from '../../@shared/services/api.service';

@Component({
    selector: 'ngx-login',
    templateUrl: './login.component.html',
})
export class NgxLoginComponent extends NbLoginComponent {

    constructor(protected service: NbAuthService,
                @Inject(NB_AUTH_OPTIONS) protected options = {},
                protected cd: ChangeDetectorRef,
                protected router: Router,
                protected api: ApiService) {
        super(service, options, cd, router);
    }

    login(): void {
        this.errors = [];
        this.messages = [];
        this.submitted = true;

        this.service.authenticate(this.strategy, this.user).subscribe((result: NbAuthResult) => {
            this.submitted = false;
            if (result.isSuccess()) {
                this.messages = result.getMessages();
                this.api.setItem('user', JSON.stringify((result.getResponse()).body.user));
            } else {
                this.errors = result.getErrors();
                if (result['response']['status'] === 403)
                    this.errors.push(result['response']['error']['error']);
                console.log(this.errors);
            }
            const redirect = result.getRedirect();
            if (redirect) {
                setTimeout(() => {
                    return this.router.navigateByUrl(redirect);
                }, this.redirectDelay);
            }
            this.cd.detectChanges();
        });
    }

    getConfigValue(key: string): any {
        return getDeepFromObject(this.options, key, null);
    }
}
