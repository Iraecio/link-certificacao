import {AbstractControl, ValidatorFn} from '@angular/forms';

export function cpfValidator(): ValidatorFn {

    return (control: AbstractControl): { [key: string]: any } | null => {
        const cpf = control.value;
        if (cpf.length > 0) {
            let numbers, digits, sum, i, result, equalDigits;
            equalDigits = 1;
            if (cpf.length < 11) {
                return {cpfNotMin: true};
            }

            if (cpf.length === 11) {
                for (i = 0; i < cpf.length - 1; i++) {
                    if (cpf.charAt(i) !== cpf.charAt(i + 1)) {
                        equalDigits = 0;
                        break;
                    }
                }

                if (!equalDigits) {
                    numbers = cpf.substring(0, 9);
                    digits = cpf.substring(9);
                    sum = 0;
                    for (i = 10; i > 1; i--) {
                        sum += numbers.charAt(10 - i) * i;
                    }

                    result = sum % 11 < 2 ? 0 : 11 - (sum % 11);

                    if (result !== Number(digits.charAt(0))) {
                        return {cpfNotValid: true};
                    }
                    numbers = cpf.substring(0, 10);
                    sum = 0;

                    for (i = 11; i > 1; i--) {
                        sum += numbers.charAt(11 - i) * i;
                    }
                    result = sum % 11 < 2 ? 0 : 11 - (sum % 11);

                    if (result !== Number(digits.charAt(1))) {
                        return {cpfNotValid: true};
                    }
                    return null;
                } else {
                    return {cpfNotValid: true};
                }
            }

            return null;
        }

        return null;
    };
}

export function cnpjValidator(): ValidatorFn {

    return (control: AbstractControl): { [key: string]: any } | null => {
        const cpf = control.value;
        let i;
        if (cpf.length > 0) {
            if (cpf.length < 14)
                return {cnpjNotMin: true};

            if (cpf.length < 14 && cpf.length > 11)
                return {cnpjNotMin: true};

            if (cpf.length === 14) {
                const cnpj = cpf.toString().replace(/\.|\-|\//g, '');
                // Se digitou o CNPJ por completo
                // Verificação do CNPJ que não respeita a regra de validação mas não são válidos
                if (cnpj === '00000000000000')
                    return {cnpjNotValid: true};

                // Calculando o Primeiro Dígito Verificador
                // Soma para o CNPJ "AB.CDE.FGH/IJKL-XZ": (A*5)+(B*4)+(C*3)+(D*2)+(E*9)+(F*8)+...+(K*3)+(L*2)
                let soma = 0;
                // multiplicador
                let mult = 5;
                for (i = 0; i < 12; i++) {
                    soma = soma + (parseInt(cnpj.charAt(i), 10) * (mult));
                    // decrementa o multiplicador
                    mult--;
                    // volta o multiplicador para o valor 9 (para decrementar até o valor 2)
                    if (mult === 1)
                        mult = 9;
                }
                // Primeiro dígito verificador (será zero se o resto da divisão de soma por 11 for < 2)
                let dv = 0;
                if ((soma % 11) > 1)
                    dv = 11 - (soma % 11);

                if (parseInt(cnpj.charAt(12), 10) !== dv)
                    return {cnpjNotValid: true};

                // Calculando o Segundo Dígito Verificador
                // Soma para o CNPJ "AB.CDE.FGH/IJKL-XZ": (A*6)+(B*5)+(C*4)+(D*3)+(E*2)+(F*9)+...+(K*4)+(L*3)+(X*2)
                soma = 0;
                // multiplicador
                mult = 6;
                for (i = 0; i < 13; i++) {
                    soma = soma + (parseInt(cnpj.charAt(i), 10) * (mult));
                    // decrementa o multiplicador
                    mult--;
                    // volta o multiplicador para o valor 9 (para decrementar até o valor 2)
                    if (mult === 1)
                        mult = 9;
                }
                // Segundo dígito verificador (será zero se o resto da divisão de soma por 11 for < 2)
                dv = 0;
                if ((soma % 11) > 1)
                    dv = 11 - (soma % 11);

                if (parseInt(cnpj.charAt(13), 10) !== dv)
                    return {cnpjNotValid: true};
            }

            return null;
        }

        return null;
    };
}

export function cpfCnpjValidator(): ValidatorFn {

    return (control: AbstractControl): { [key: string]: any } | null => {
        const cpf = control.value;
        if (cpf.length > 0) {
            let numbers, digits, sum, i, result, equalDigits;
            equalDigits = 1;
            if (cpf.length < 11) {
                return {cpfNotMin: true};
            }

            if (cpf.length < 14 && cpf.length > 11) {
                return {cnpjNotMin: true};
            }

            if (cpf.length === 11) {
                for (i = 0; i < cpf.length - 1; i++) {
                    if (cpf.charAt(i) !== cpf.charAt(i + 1)) {
                        equalDigits = 0;
                        break;
                    }
                }

                if (!equalDigits) {
                    numbers = cpf.substring(0, 9);
                    digits = cpf.substring(9);
                    sum = 0;
                    for (i = 10; i > 1; i--) {
                        sum += numbers.charAt(10 - i) * i;
                    }

                    result = sum % 11 < 2 ? 0 : 11 - (sum % 11);

                    if (result !== Number(digits.charAt(0))) {
                        return {cpfNotValid: true};
                    }
                    numbers = cpf.substring(0, 10);
                    sum = 0;

                    for (i = 11; i > 1; i--) {
                        sum += numbers.charAt(11 - i) * i;
                    }
                    result = sum % 11 < 2 ? 0 : 11 - (sum % 11);

                    if (result !== Number(digits.charAt(1))) {
                        return {cpfNotValid: true};
                    }
                    return null;
                } else {
                    return {cpfNotValid: true};
                }
            }

            if (cpf.length === 14) {
                const cnpj = cpf.toString().replace(/\.|\-|\//g, '');
                // Se digitou o CNPJ por completo
                // Verificação do CNPJ que não respeita a regra de validação mas não são válidos
                if (cnpj === '00000000000000')
                    return {cnpjNotValid: true};

                // Calculando o Primeiro Dígito Verificador
                // Soma para o CNPJ "AB.CDE.FGH/IJKL-XZ": (A*5)+(B*4)+(C*3)+(D*2)+(E*9)+(F*8)+...+(K*3)+(L*2)
                let soma = 0;
                // multiplicador
                let mult = 5;
                for (i = 0; i < 12; i++) {
                    soma = soma + (parseInt(cnpj.charAt(i), 10) * (mult));
                    // decrementa o multiplicador
                    mult--;
                    // volta o multiplicador para o valor 9 (para decrementar até o valor 2)
                    if (mult === 1)
                        mult = 9;
                }
                // Primeiro dígito verificador (será zero se o resto da divisão de soma por 11 for < 2)
                let dv = 0;
                if ((soma % 11) > 1)
                    dv = 11 - (soma % 11);

                if (parseInt(cnpj.charAt(12), 10) !== dv)
                    return {cnpjNotValid: true};

                // Calculando o Segundo Dígito Verificador
                // Soma para o CNPJ "AB.CDE.FGH/IJKL-XZ": (A*6)+(B*5)+(C*4)+(D*3)+(E*2)+(F*9)+...+(K*4)+(L*3)+(X*2)
                soma = 0;
                // multiplicador
                mult = 6;
                for (i = 0; i < 13; i++) {
                    soma = soma + (parseInt(cnpj.charAt(i), 10) * (mult));
                    // decrementa o multiplicador
                    mult--;
                    // volta o multiplicador para o valor 9 (para decrementar até o valor 2)
                    if (mult === 1)
                        mult = 9;
                }
                // Segundo dígito verificador (será zero se o resto da divisão de soma por 11 for < 2)
                dv = 0;
                if ((soma % 11) > 1)
                    dv = 11 - (soma % 11);

                if (parseInt(cnpj.charAt(13), 10) !== dv)
                    return {cnpjNotValid: true};
            }

            return null;
        }

        return null;
    };
}
