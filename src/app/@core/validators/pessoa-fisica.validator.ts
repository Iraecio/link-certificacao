import {AbstractControl, ValidatorFn} from '@angular/forms';

export function PessoaFisicaRequired(tipo: string = '1'): ValidatorFn {

    return (control: AbstractControl): { [key: string]: any } | null => {
        const value = control.value;

        if (tipo === '1' && value.length === 0)
            return {required: true};

        return null;
    };
}
