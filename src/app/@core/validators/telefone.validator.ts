import {FormGroup} from '@angular/forms';

export class TelefoneValidator {
    static validate(TelefoneFormGroup: FormGroup) {
        const telefone_residencial = TelefoneFormGroup.controls.residencial.value;
        const telefone_comercial = TelefoneFormGroup.controls.comercial.value;
        const telefone_celular = TelefoneFormGroup.controls.celular.value;
        const telefone_fax = TelefoneFormGroup.controls.fax.value;
        if (telefone_residencial.length === 0
            && telefone_comercial.length === 0
            && telefone_celular.length === 0
            && telefone_fax.length === 0) {
            return {telefoneNeeded: true};
        }

        return null;
    }
}
