import {FormGroup} from '@angular/forms';

export class PasswordValidator {

  static validate(passwordFormGroup: FormGroup) {
    const password = passwordFormGroup.controls.password.value;
    const repeatPassword = passwordFormGroup.controls.password_confirm.value;

    // Confirma a senha
    if (password.length <= 0)
      return null;

    if (password.length > 0 && repeatPassword.length <= 0)
      return {confirmPassword: true};

    if (repeatPassword !== password)
      return {doesMatchPassword: true};

    return null;

  }
}
