import {AbstractControl, ValidatorFn} from '@angular/forms';

export function PessoaJuridicaRequired(tipo: string = '1'): ValidatorFn {

    return (control: AbstractControl): { [key: string]: any } | null => {
        const value = control.value;

        if (tipo === '2' && value.length === 0)
            return {required: true};

        return null;
    };
}
