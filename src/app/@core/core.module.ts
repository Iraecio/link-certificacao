import {
    ModuleWithProviders,
    NgModule,
    Optional,
    SkipSelf,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import {
    HTTP_INTERCEPTORS,
    HttpClientModule,
    HttpErrorResponse,
} from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {
    NbAuthJWTToken,
    NbAuthModule,
    NbPasswordAuthStrategy,
    NbPasswordAuthStrategyOptions,
} from '@nebular/auth';
import { environment } from '../../environments/environment';
import { NgxAuthModule } from '../auth/auth.module';
import { getDeepFromObject } from '@nebular/auth/helpers';
import { TokenInterceptor } from './token.interceptor';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxAlertsModule } from '@ngx-plus/ngx-alerts';

export function geter(
    module: string,
    res: HttpErrorResponse,
    options: NbPasswordAuthStrategyOptions
) {
    getDeepFromObject(
        res.error,
        options.errors.key,
        options[module].defaultErrors
    );
}

import { throwIfAlreadyLoaded } from './module-import-guard';

export const NB_CORE_PROVIDERS = [
    ...NbAuthModule.forRoot({
        strategies: [
            NbPasswordAuthStrategy.setup({
                name: 'cpf',
                baseEndpoint: environment.baseEndpoint,
                errors: {
                    key: 'error.errors',
                    getter: geter,
                },
                login: {
                    endpoint: 'auth/login',
                    method: 'post',
                    defaultErrors: ['Credenciais estão incorretas.'],
                    defaultMessages: ['Login efetuado com sucesso.'],
                },
                register: {
                    endpoint: 'auth/signup',
                    method: 'post',
                    defaultErrors: ['Error ao efetuar o cadastro.'],
                    defaultMessages: ['Cadastro efetuado com sucesso.'],
                },
                logout: {
                    endpoint: 'auth/logout',
                    method: 'post',
                    redirect: {
                        success: 'auth/login',
                        failure: '/',
                    },
                    requireValidToken: true,
                },
                requestPass: {
                    endpoint: 'auth/request-pass',
                    method: 'post',
                },
                resetPass: {
                    endpoint: 'auth/reset-pass',
                    method: 'post',
                },
                refreshToken: {
                    endpoint: 'auth/refresh',
                    method: 'post',
                },
                token: {
                    class: NbAuthJWTToken,
                    key: 'token',
                },
            }),
        ],
        forms: {
            login: {
                redirectDelay: 500,
                strategy: 'cpf',
                rememberMe: true,
                showMessages: {
                    success: true,
                    error: true,
                },
            },
            logout: {
                redirectDelay: 500,
                strategy: 'cpf',
            },
            register: {
                redirectDelay: 500,
                strategy: 'cpf',
                showMessages: {
                    success: true,
                    error: true,
                },
                terms: true,
            },
            requestPassword: {
                redirectDelay: 500,
                strategy: 'cpf',
                showMessages: {
                    success: true,
                    error: true,
                },
            },
            resetPassword: {
                redirectDelay: 500,
                strategy: 'cpf',
                showMessages: {
                    success: true,
                    error: true,
                },
            },
            validation: {
                password: {
                    required: true,
                    minLength: 8,
                    maxLength: 30,
                },
                cpf: {
                    required: true,
                    minLength: 11,
                    maxLength: 14,
                },
                fullName: {
                    required: false,
                    minLength: 4,
                    maxLength: 50,
                },
            },
        },
    }).providers,
    ...NgxAlertsModule.forRoot().providers,
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
];

const COREMODULES = [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    NgbModule,
    NbAuthModule,
    NgxAuthModule,
    NgxAlertsModule,
];
@NgModule({
    declarations: [],
    imports: [CommonModule, ...COREMODULES],
    exports: [...COREMODULES],
    providers: [],
})
export class CoreModule {
    constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
        throwIfAlreadyLoaded(parentModule, 'CoreModule');
    }

    static forRoot(): ModuleWithProviders {
        return <ModuleWithProviders>{
            ngModule: CoreModule,
            providers: [...NB_CORE_PROVIDERS],
        };
    }
}
