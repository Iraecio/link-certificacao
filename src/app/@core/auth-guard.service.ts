import { Injectable, Injector } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { NbAuthService } from '@nebular/auth';
import { tap } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
    constructor(private injector: Injector) {}

    protected get router(): Router {
        return this.injector.get(Router);
    }

    protected get authService(): NbAuthService {
        return this.injector.get(NbAuthService);
    }

    canActivate() {
        return this.authService.isAuthenticated().pipe(
            tap(authenticated => {
                if (!authenticated) this.router.navigate(['/auth/login']);
            })
        );
    }
}
