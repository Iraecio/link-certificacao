import {Injectable, Injector} from '@angular/core';
import {
    HttpErrorResponse,
    HttpEvent,
    HttpHandler,
    HttpInterceptor,
    HttpRequest,
    HttpResponse
} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {NbAuthService, NbAuthToken, NbTokenService} from '@nebular/auth';
import {environment} from '../../environments/environment';
import {tap} from 'rxjs/operators';
import {Router} from '@angular/router';

@Injectable({providedIn: 'root'})
export class TokenInterceptor implements HttpInterceptor {

    constructor(private injector: Injector) {
    }

    protected get router(): Router {
        return this.injector.get(Router);
    }

    protected get authService(): NbAuthService {
        return this.injector.get(NbAuthService);
    }

    protected get tokenService(): NbTokenService {
        return this.injector.get(NbTokenService);
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        if (request.url === environment.baseEndpoint + 'auth/logout') {
            if (request.method === 'POST') {
                this.authService.getToken().subscribe((v: any) => {
                    request = request.clone({body: {token: v.token}});
                });
            }
        }

        return next.handle(request).pipe(
            tap((event: HttpEvent<any>) => {
                if (event instanceof HttpResponse) {
                    if (event['url'].indexOf(environment.baseEndpoint) > -1) {
                        this.authService.getToken().subscribe((res: NbAuthToken) => {
                            let token: string = event.headers.get('authorization');
                            if (null !== token) {
                                token = token.substring(7);
                                res['token'] = token;
                                this.tokenService.set(res);
                            }
                        });
                    }
                }

            }, (error: any) => {
                if (error instanceof HttpErrorResponse) {
                    if (error['url'].indexOf(environment.baseEndpoint) > -1 && error.status === 401)
                        this.router.navigate(['auth/login']);
                }
            })
        );
    }
}
