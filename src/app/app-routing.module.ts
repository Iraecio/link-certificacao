import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {AuthGuard} from './@core/auth-guard.service';

const routes: Routes = [
    {path: 'sistema', canActivate: [AuthGuard], loadChildren: 'app/pages/pages.module#PagesModule'},
    {path: 'auth', loadChildren: './auth/auth.module#NgxAuthModule'},
    {path: '', redirectTo: 'sistema', pathMatch: 'full'},
    {path: '**', redirectTo: 'sistema'},
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRoutingModule {
}

