export class FaturaResponse {
    id: string;
    due_date: string;
    currency: string;
    discount_cents: string | null;
    email: string;
    items_total_cents: number;
    notification_url: string | null;
    return_url: string | null;
    status: string;
    tax_cents: string | null;
    updated_at: string;
    total_cents: number;
    total_paid_cents: number;
    paid_at: string | null;
    taxes_paid_cents: string | null;
    paid_cents: string | null;
    cc_emails: string | null;
    financial_return_date: string | null;
    payable_with: string;
    overpaid_cents: string | null;
    ignore_due_email: string | null;
    ignore_canceled_email: string | null;
    advance_fee_cents: string | null;
    commission_cents: string | null;
    early_payment_discount: string | false;
    order_id: string | null;
    secure_id: string;
    secure_url: string;
    customer_id: string | null;
    customer_ref: string | null;
    customer_name: string | null;
    user_id: string | null;
    total: string;
    taxes_paid: string;
    total_paid: string;
    total_overpaid: string;
    commission: string;
    fines_on_occurrence_day: string | null;
    total_on_occurrence_day: string | null;
    fines_on_occurrence_day_cents: string | null;
    total_on_occurrence_day_cents: string | null;
    advance_fee: string | null;
    paid: string;
    original_payment_id: string | null;
    double_payment_id: string | null;
    interest: string | null;
    discount: string | null;
    created_at: string;
    refundable: string | null;
    installments: string | null;
    transaction_number: number;
    payment_method: string | null;
    created_at_iso: string;
    updated_at_iso: string;
    occurrence_date: string | null;
    financial_return_dates: string | null;
    bank_slip: BankSlip;
    items: Array<Item>;
    early_payment_discounts: Array<object>;
    variables: Array<Variable>;
    custom_variables: Array<CustomVariables>;
    logs: Array<Log>;
}

export class BankSlip {
    digitable_line: string;
    barcode_data: string;
    barcode: string;
}

export class Item {
    id: string;
    description: string;
    price_cents: number;
    quantity: number;
    created_at: string;
    updated_at: string;
    price: string;
}

export class Variable {
    id: string;
    variable: string;
    value: string;
}

export class CustomVariables {
    id: string;
    name: string;
    value: string;
}

export class Log {
    id: string;
    description: string;
    notes: string;
    created_at: string;
}
