export class ServerSourceConf {

    protected static readonly SORT_FIELD_KEY = 'sort';
    protected static readonly SORT_FIELD_EXCLUDE_KEY = [];
    protected static readonly SORT_DIR_KEY = 'order';
    protected static readonly PAGER_PAGE_KEY = 'page[number]';
    protected static readonly PAGER_LIMIT_KEY = 'page[size]';
    protected static readonly FILTER_FIELD_KEY = '#field#_like';
    protected static readonly TOTAL_KEY = 'total';
    protected static readonly DATA_KEY = 'data';
    protected static readonly INCLUDE_KEY = 'include';
    endPoint: string;

    sortFieldKey?: string;
    sortFieldExcludeKey?: Array<any>;
    sortDirKey?: string;
    pagerPageKey?: string;
    pagerLimitKey?: string;
    filterFieldKey?: string;
    totalKey?: string;
    dataKey?: string;
    includeKey?: string;
    includeFieldKey?: string;

    constructor(
        {
            endPoint = '', sortFieldKey = '', sortFieldExcludeKey  = [], sortDirKey = '',
            pagerPageKey = '', pagerLimitKey = '', filterFieldKey = '', totalKey = '', dataKey = '', includeKey = '', includeFieldKey = ''
        } = {}) {

        this.endPoint = endPoint ? endPoint : '';

        this.sortFieldKey = sortFieldKey ? sortFieldKey : ServerSourceConf.SORT_FIELD_KEY;
        this.sortFieldExcludeKey = sortFieldExcludeKey ? sortFieldExcludeKey : ServerSourceConf.SORT_FIELD_EXCLUDE_KEY;
        this.sortDirKey = sortDirKey ? sortDirKey : ServerSourceConf.SORT_DIR_KEY;
        this.pagerPageKey = pagerPageKey ? pagerPageKey : ServerSourceConf.PAGER_PAGE_KEY;
        this.pagerLimitKey = pagerLimitKey ? pagerLimitKey : ServerSourceConf.PAGER_LIMIT_KEY;
        this.filterFieldKey = filterFieldKey ? filterFieldKey : ServerSourceConf.FILTER_FIELD_KEY;
        this.totalKey = totalKey ? totalKey : ServerSourceConf.TOTAL_KEY;
        this.dataKey = dataKey ? dataKey : ServerSourceConf.DATA_KEY;
        this.includeKey = includeKey ? includeKey : ServerSourceConf.INCLUDE_KEY;
        this.includeFieldKey = includeFieldKey ? includeFieldKey : '';
    }
}
