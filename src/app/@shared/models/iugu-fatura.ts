export class Fatura {
    pedido_id: any;
    email: string;
    cc_emails: string;
    due_date: string;
    ensure_workday_due_date: boolean;
    items: Array<Items>;
    return_url: string;
    expired_url: string;
    notification_url: string;
    fines: boolean;
    late_payment_fine: number;
    per_day_interest: boolean;
    discount_cents: number;
    customer_id: string;
    ignore_due_email: boolean;
    subscription_id: string;
    payable_with: string;
    credits: number;
    custom_variables: Array<CustomVariables>;
    early_payment_discount: boolean;
    early_payment_discounts: Array<EarlyPaymentDiscounts>;
    payer: Payer;
    order_id: string;
    logs: Log;
}

export class Items {
    description: string;
    quantity: number;
    price_cents: number;
}

export class CustomVariables {
    name: string;
    value: string;
}

export class EarlyPaymentDiscounts {
    days: number;
    percent: string;
    value_cents: number;
}

export class Payer {
    cpf_cnpj: string;
    name: string;
    phone_prefix: string;
    phone: string;
    email: string;
    address: Address;
}

export class Address {
    zip_code: string;
    street: string;
    number: string;
    district: string;
    city: string;
    state: string;
    country: string;
    complement: string;
}

export class Log {
    description: string;
    notes: string;
}
