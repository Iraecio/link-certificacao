import {Input, Directive, ElementRef, HostListener} from '@angular/core';

@Directive({selector: '[ngxMyNumberOnly]'})
export class NumberOnlyDirective {

    constructor(private _el: ElementRef) {
    }

    @HostListener('input', ['$event'])
    onInputChange(event) {
        const initalValue = this._el.nativeElement.value;
        this._el.nativeElement.value = initalValue.replace(/[^0-9\/]*/g, '');
        if (initalValue !== this._el.nativeElement.value)
            event.stopPropagation();
    }
}


@Directive({selector: '[ngxNumberZeroToCent]'})
export class NumberZeroToCentDirective {

    @Input('ngxNumberZeroToCent') maximo: number;

    constructor(private _el: ElementRef) {
    }

    @HostListener('input', ['$event'])
    onInputChange(event) {
        const initalValue = this._el.nativeElement.value;
        const valor = initalValue.replace(/[^0-9\/]*/g, '');

        if (+valor > this.maximo) {
            this._el.nativeElement.value = this.maximo;
        } else {
            this._el.nativeElement.value = valor;
        }

        if (initalValue !== this._el.nativeElement.value)
            event.stopPropagation();
    }
}
