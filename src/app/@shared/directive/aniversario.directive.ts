import {Directive, HostListener, ElementRef} from '@angular/core';

@Directive({selector: '[ngxAniversario]'})
export class AniversarioDirective {

    constructor(private _el: ElementRef) {
    }

    @HostListener('input', ['$event'])
    onInputChange(event) {
        const v = this._el.nativeElement.value;
        if (v.match(/^\d{2}$/) !== null) {
            return this._el.nativeElement.value = v + '/';
        } else if (v.match(/^\d{2}\/\d{2}$/) !== null) {
            return this._el.nativeElement.value = v + '/';
        } else if (v.match(/^\d{2}\/\d{2}\/\d{4}$/) !== null) {
            return this._el.nativeElement.value = v;
        } else if (v.match(/^\d{8}$/) !== null) {
            return this._el.nativeElement.value = v.substring(0, 2) + '/' + v.substring(2, 4) + '/' + v.substring(4, 8);
        }
    }
}
