import {HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {LocalDataSource} from 'ng2-smart-table';
import {getDeepFromObject} from 'ng2-smart-table/lib/helpers';
import {ServerSourceConf} from '../models/server-data-source.model';
import {ApiService} from './api.service';
import {Injector} from '@angular/core';

export class ServerDataSource extends LocalDataSource {

    protected conf: ServerSourceConf;

    protected lastRequestCount: number = 0;

    constructor(protected injector: Injector, conf: ServerSourceConf | {} = {}) {
        super();

        this.conf = new ServerSourceConf(conf);

        if (!this.conf.endPoint) {
            throw new Error('Pelo menos, o endPoint deve ser especificado como uma configuração da fonte de dados do servidor.');
        }
    }

    protected get api(): ApiService {
        return this.injector.get(ApiService);
    }

    count(): number {
        return this.lastRequestCount;
    }

    getElements(): Promise<any> {
        return this.requestElements()
            .pipe(map(res => {
                this.lastRequestCount = this.extractTotalFromResponse(res);
                this.data = this.extractDataFromResponse(res);
                return this.data;
            })).toPromise();
    }

    /**
     * Extracts array of data from server response
     * @param res
     * @returns {any}
     */
    protected extractDataFromResponse(res: any): Array<any> {
        const data = !!this.conf.dataKey ? getDeepFromObject(res, this.conf.dataKey, []) : res;

        if (data instanceof Array) {
            return data;
        }

        throw new Error(`Os dados devem ser uma matriz.
     Por favor, verifique se os dados extraídos da resposta do servidor pela chave '${this.conf.dataKey}' existe e é matriz.`);
    }

    /**
     * Extracts total rows count from the server response
     * Looks for the count in the heders first, then in the response body
     * @param res
     * @returns {any}
     */
    protected extractTotalFromResponse(res: any): number {
        return getDeepFromObject(res, this.conf.totalKey, 0);
    }

    protected requestElements(): Observable<any> {
        const httpParams = this.createRequesParams();
        return this.api.getForTable(this.conf.endPoint, httpParams);
    }

    protected createRequesParams(): HttpParams {
        let httpParams = new HttpParams();

        httpParams = this.addSortRequestParams(httpParams);
        httpParams = this.addFilterRequestParams(httpParams);
        httpParams = this.addIncludeRequestParams(httpParams);
        return this.addPagerRequestParams(httpParams);
    }

    protected addIncludeRequestParams(httpParams: HttpParams): HttpParams {
        if (this.conf.includeFieldKey) {
            httpParams = httpParams.set(this.conf.includeKey, this.conf.includeFieldKey);
        }

        return httpParams;
    }

    protected addSortRequestParams(httpParams: HttpParams): HttpParams {
        if (this.sortConf) {
            this.sortConf.forEach((fieldConf) => {
                const direcao = fieldConf.direction.toUpperCase();
                const order = direcao === 'DESC' ? '-' : '';
                if (!(this.conf.sortFieldExcludeKey.indexOf(fieldConf.field) > -1))
                    httpParams = httpParams.set(this.conf.sortFieldKey, order + fieldConf.field);
            });
        }

        return httpParams;
    }

    protected addFilterRequestParams(httpParams: HttpParams): HttpParams {

        if (this.filterConf.filters) {
            this.filterConf.filters.forEach((fieldConf: any) => {
                if (fieldConf['search']) {
                    httpParams = httpParams.set(this.conf.filterFieldKey.replace('#field#', fieldConf['field']), fieldConf['search']);
                }
            });
        }

        return httpParams;
    }

    protected addPagerRequestParams(httpParams: HttpParams): HttpParams {

        if (this.pagingConf && this.pagingConf['page'] && this.pagingConf['perPage']) {
            httpParams = httpParams.set(this.conf.pagerPageKey, this.pagingConf['page']);
            httpParams = httpParams.set(this.conf.pagerLimitKey, this.pagingConf['perPage']);
        }

        return httpParams;
    }
}
