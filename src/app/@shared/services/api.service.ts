import { Injectable, Injector } from '@angular/core';
import {
    HttpClient,
    HttpErrorResponse,
    HttpHeaders,
    HttpParams,
} from '@angular/common/http';
import { Observable, Subject, throwError } from 'rxjs';
import { catchError, retry, tap } from 'rxjs/operators';
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { environment } from '../../../environments/environment';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};

@Injectable({ providedIn: 'root' })
export class ApiService {
    protected http: HttpClient;
    protected authService: NbAuthService;
    private url: string = environment.baseEndpoint;
    private urlWs: string = environment.wsEndpoint;
    private token: any;

    private storageSub = new Subject<any>();

    constructor(protected injector: Injector) {
        this.http = this.injector.get(HttpClient);
        this.authService = this.injector.get(NbAuthService);

        this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
            if (token.isValid()) {
                this.authService.getToken().subscribe(res => {
                    this.token = res.getValue();
                    ApiService.httpsOptions({ params: { token: this.token } });
                });
            }
        });
    }

    getForTable(url: string, httpParamers: HttpParams): Observable<any> {
        const httpOptionsTable = {
            headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
            params: { token: this.token },
        };
        return this.http.get(
            `${this.url}${url}${
                httpParamers ? '?' + httpParamers.toString() : ''
            }`,
            httpOptionsTable
        );
    }

    get(url: string, id?: string): Observable<any> {
        return this.http
            .get<any>(`${this.url}${url}${id ? '/' + id : ''}`, httpOptions)
            .pipe(
                retry(3),
                tap(res => ApiService.handleResponse(res)),
                catchError(ApiService.handleError)
            );
    }

    post(url: string, data): Observable<any> {
        return this.http.post<any>(`${this.url}${url}`, data, httpOptions).pipe(
            retry(3),
            tap(res => ApiService.handleResponse(res)),
            catchError(ApiService.handleError)
        );
    }

    update(url: string, id: string, data): Observable<any> {
        return this.http
            .put(`${this.url}${url}${id ? '/' + id : ''}`, data, httpOptions)
            .pipe(
                retry(3),
                tap(res => ApiService.handleResponse(res)),
                catchError(ApiService.handleError)
            );
    }

    delete(url: string, id: string): Observable<any> {
        return this.http
            .delete<any>(`${this.url}${url}${id ? '/' + id : ''}`, httpOptions)
            .pipe(
                retry(3),
                tap(res => ApiService.handleResponse(res)),
                catchError(ApiService.handleError)
            );
    }

    /*===============================================================
     *               Webservice Link Certificação
     **===============================================================*/
    postWs(data: URLSearchParams): Observable<any> {
        const Options = {
            headers: new HttpHeaders({
                'Content-Type': 'application/x-www-form-urlencoded',
            }),
        };
        data.append('seguranca[cnpj_cpf]', environment.wsCpf);
        data.append('seguranca[token]', environment.wsToken);
        return this.http
            .post<any>(`${this.urlWs}`, data.toString(), Options)
            .pipe(
                retry(3),
                tap(res => ApiService.handleResponse(res)),
                catchError(ApiService.handleError)
            );
    }

    /*===============================================================
     *               User Service
     **===============================================================*/
    watchStorage(): Observable<any> {
        return this.storageSub.asObservable();
    }

    setItem(key: string, data: any) {
        localStorage.setItem(key, data);
        this.storageSub.next('changed');
    }

    /*===============================================================
     *               Private methods
     **===============================================================*/
    private static httpsOptions(args: Object): void {
        Object.assign(httpOptions, args);
    }

    private static handleError(error: HttpErrorResponse) {
        console.error(error);
        if (error.error instanceof ErrorEvent) {
            console.error('Houve um error:', error.error.message);
        } else {
            console.error(
                `Servidor retornou error ${error.status}, ` +
                    `Corpo do error: ${error.error.error.message}`
            );
        }
        // return an observable with a user-facing error message
        return throwError(error);
    }

    private static handleResponse(response) {
        if (!environment.production) console.log(response);
    }
}
