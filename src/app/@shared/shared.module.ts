import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {NumberOnlyDirective, NumberZeroToCentDirective} from './directive/number.directive';
import {AniversarioDirective} from './directive/aniversario.directive';

const SHARED_DIRECTIVE = [
    NumberOnlyDirective,
    NumberZeroToCentDirective,
    AniversarioDirective
];

@NgModule({
    imports: [
        CommonModule,
    ],
    exports: [
        CommonModule,
        ...SHARED_DIRECTIVE
    ],
    declarations: [
        ...SHARED_DIRECTIVE
    ],
    providers: [

    ]
})

export class SharedModule {
}
