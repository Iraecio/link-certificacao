import {NgxAlertsService} from '@ngx-plus/ngx-alerts';
import {BaseResourceModel} from '../models/base-resource.model';
import {Injector, OnDestroy} from '@angular/core';
import {ApiService} from '../services/api.service';
import {FormBuilder} from '@angular/forms';
import {Router} from '@angular/router';
import {Subject} from 'rxjs';
import {environment} from '../../../environments/environment.prod';

export abstract class BaseResourceComponent<T extends BaseResourceModel> implements OnDestroy {

    loading: boolean = false;
    user: any;
    entityErrors: Array<{ campo: string, text: string }> = [];

    ngUnsubscribe = new Subject();

    protected constructor(protected injector: Injector) {
        this.user = JSON.parse(localStorage.getItem('user'));
        this.api.watchStorage().subscribe(() => {
            this.user = JSON.parse(localStorage.getItem('user'));
        });
    }

    protected get alerts(): NgxAlertsService {
        return this.injector.get(NgxAlertsService);
    }

    protected get api(): ApiService {
        return this.injector.get(ApiService);
    }

    protected get fb(): FormBuilder {
        return this.injector.get(FormBuilder);
    }

    protected get _route(): Router {
        return this.injector.get(Router);
    }

    protected load() {
        this.loading = true;
    }

    protected loaded() {
        this.loading = false;
    }

    protected error(error) {
        this.log(error);
        if (error.status === 500) {
            this.alerts.alertError({title: 'Oops', text: 'Erro no servidor, tente novamente mais tarde.'}).then();
        }
        if (error.status === 422) {
            this.entityErrors = [];
            const err = error.error.error.errors;
            Object.keys(err).forEach(input => {
                err[input].forEach(value => {
                    this.entityErrors.push({campo: input, text: value});
                });
            });
        }
    }

    ngOnDestroy(): void {
        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    }

    protected log(...arg) {
        if (!environment.production)
            console.log(arg);
    }
}
