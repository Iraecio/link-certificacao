import {Component} from '@angular/core';

@Component({
    selector: 'ngx-footer',
    styleUrls: ['./footer.component.scss'],
    template: `<span class="created-by">Criado com ♥ <b>2019</b></span>`,
})
export class FooterComponent {
}
