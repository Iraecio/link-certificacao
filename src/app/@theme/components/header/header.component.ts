import {Component, Injector, Input} from '@angular/core';
import {NbMenuService, NbSidebarService} from '@nebular/theme';
import {BaseResourceComponent} from '../../../@shared/components/base-resource.component';

@Component({
    selector: 'ngx-header',
    styleUrls: ['./header.component.scss'],
    templateUrl: './header.component.html',
})
export class HeaderComponent extends BaseResourceComponent<any> {

    @Input() position = 'normal';

    userMenu = [
        {title: 'Perfil', link: '/sistema/user/perfil'},
        {title: 'Sair', link: '/auth/logout'}
    ];

    constructor(private sidebarService: NbSidebarService,
                private menuService: NbMenuService,
                protected injector: Injector) {
        super(injector);
    }

    toggleSidebar(): boolean {
        this.sidebarService.toggle(true, 'menu-sidebar');
        return false;
    }

    goToHome() {
        this.menuService.navigateHome();
    }

}
