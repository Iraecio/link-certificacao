import { Component, Injector, OnInit } from '@angular/core';
import { BaseResourceComponent } from '../../../@shared/components/base-resource.component';
import { ServerSourceConf } from '../../../@shared/models/server-data-source.model';
import { ServerDataSource } from '../../../@shared/services/server.data-source';
import { ClientesDataSource } from '../services/clientes.data-source';

@Component({
    selector: 'ngx-clientes',
    template: `
        <div class="row">
            <div class="col-lg-12">
                <nb-card>
                    <nb-card-header>Clientes</nb-card-header>
                    <nb-card-body>
                        <ng2-smart-table
                            [settings]="settings"
                            [source]="source"
                            (create)="onCreate($event)"
                            (edit)="onEdit($event)"
                        >
                        </ng2-smart-table>
                    </nb-card-body>
                </nb-card>
            </div>
        </div>
    `,
})
export class ClientesComponent extends BaseResourceComponent<any> {
    settings = {
        add: {
            addButtonContent: '<i class="nb-plus"></i>',
            createButtonContent: '<i class="nb-checkmark"></i>',
            cancelButtonContent: '<i class="nb-close"></i>',
        },
        edit: {
            editButtonContent: '<i class="nb-edit"></i>',
            saveButtonContent: '<i class="nb-checkmark"></i>',
            cancelButtonContent: '<i class="nb-close"></i>',
        },
        mode: 'external',
        columns: {
            nome: {
                title: 'Nome/Razão',
                type: 'string',
            },
            email: {
                title: 'Email',
                type: 'string',
            },
            cpf: {
                title: 'Cpf',
                type: 'string',
            },
        },
        sort: true,
        sortDirection: 'desc',
        pager: {
            perPage: 10,
        },
        actions: {
            columnTitle: 'AÇÕES',
            position: 'right',
            add: true,
            edit: true,
            delete: false,
        },
    };

    tableConf: ServerSourceConf = {
        endPoint: 'pessoa',
    };

    source: ServerDataSource;

    constructor(protected injector: Injector) {
        super(injector);
        this.source = new ClientesDataSource(injector, this.tableConf);
    }

    onEdit(event) {
        this._route
            .navigate(['/sistema/clientes/' + event.data.id + '/view'])
            .then();
    }

    onCreate(event) {
        this._route.navigate(['/sistema/clientes/add']).then();
    }
}
