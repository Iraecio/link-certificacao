import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClientesComponent } from './clientes/clientes.component';
import { ClienteComponent } from './cliente.component';
import { ThemeModule } from '../../@theme/theme.module';
import { SharedModule } from '../../@shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ClienteRoutingModule } from './cliente-routing.module';
import { ClientesAddComponent } from './cliente-add/cliente-add.component';
import { ClientesViewComponent } from './cliente-view/cliente-view.component';

const CLIENTECOMPONENT = [
    ClienteComponent,
    ClientesComponent,
    ClientesAddComponent,
    ClientesViewComponent,
];

const ENTRYCOMPONENT = [];

@NgModule({
    declarations: [...CLIENTECOMPONENT],
    entryComponents: [...ENTRYCOMPONENT],
    imports: [
        ClienteRoutingModule,
        CommonModule,
        ThemeModule,
        SharedModule,
        ReactiveFormsModule,
        Ng2SmartTableModule,
    ],
})
export class ClienteModule {}
