import { Component, Injector, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { BaseResourceComponent } from '../../../@shared/components/base-resource.component';
import { Observable } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { TelefoneValidator } from '../../../@core/validators/telefone.validator';
import { PessoaJuridicaRequired } from '../../../@core/validators/pessoa-juridica.validator';
import { PessoaFisicaRequired } from '../../../@core/validators/pessoa-fisica.validator';
import {
    cnpjValidator,
    cpfValidator,
} from '../../../@core/validators/cpf-cnpj.validator';

@Component({
    selector: 'ngx-cliente-add',
    templateUrl: './cliente-add.component.html',
    styleUrls: ['./cliente-add.component.scss'],
})
export class ClientesAddComponent extends BaseResourceComponent<any>
    implements OnInit {
    pessoaForm: FormGroup;
    tipoForm: string = '1';
    estados = [
        { id: 'AC', nome: 'Acre' },
        { id: 'AL', nome: 'Alagoas' },
        { id: 'AP', nome: 'Amapá' },
        { id: 'AM', nome: 'Amazonas' },
        { id: 'BA', nome: 'Bahia' },
        { id: 'CE', nome: 'Ceará' },
        { id: 'DF', nome: 'Distrito Federal' },
        { id: 'ES', nome: 'Espírito Santo' },
        { id: 'GO', nome: 'Goiás' },
        { id: 'MA', nome: 'Maranhão' },
        { id: 'MT', nome: 'Mato Grosso' },
        { id: 'MS', nome: 'Mato Grosso do Sul' },
        { id: 'MG', nome: 'Minas Gerais' },
        { id: 'PA', nome: 'Pará' },
        { id: 'PB', nome: 'Paraíba' },
        { id: 'PR', nome: 'Paraná' },
        { id: 'PE', nome: 'Pernambuco' },
        { id: 'PI', nome: 'Piauí' },
        { id: 'RJ', nome: 'Rio de Janeiro' },
        { id: 'RN', nome: 'Rio Grande do Norte' },
        { id: 'RS', nome: 'Rio Grande do Sul' },
        { id: 'RO', nome: 'Rondônia' },
        { id: 'RR', nome: 'Roraima' },
        { id: 'SC', nome: 'Santa Catarina' },
        { id: 'SP', nome: 'São Paulo' },
        { id: 'SE', nome: 'Sergipe' },
        { id: 'TO', nome: 'Tocantins' },
    ];
    cidades;
    private pessoa: object = {};
    private telefone: object = {};
    private endereco: object = {};
    private pessoaFisica: object = {};
    private pessoaJuridica: object = {};

    constructor(protected injector: Injector) {
        super(injector);
    }

    ngOnInit() {
        this.pessoaForm = this.fb.group({
            tipo: ['', Validators.required],
            nome: ['', Validators.required],
            email: ['', [Validators.required, Validators.email]],
            observacao: [''],
            pis: [''],
            pessoa_fisica: this.fb.group({
                cpf: [
                    '',
                    [PessoaFisicaRequired.bind(this.tipoForm), cpfValidator()],
                ],
                rg: [''],
                orgao_expedidor: ['', [Validators.maxLength(10)]],
                data_expedicao: [''],
                data_nascimento: ['', PessoaFisicaRequired.bind(this.tipoForm)],
                cei: [''],
                cargo: ['', PessoaFisicaRequired.bind(this.tipoForm)],
            }),
            pessoa_juridica: this.fb.group({
                cnpj: [
                    '',
                    [
                        PessoaJuridicaRequired.bind(this.tipoForm),
                        cnpjValidator(),
                    ],
                ],
                ie: [''],
                im: [''],
                nome_fantasia: [''],
                substituto: [''],
                website: [''],
                contato_nome: ['', PessoaJuridicaRequired.bind(this.tipoForm)],
                contato_telefone: [
                    '',
                    PessoaJuridicaRequired.bind(this.tipoForm),
                ],
                contato_email: ['', PessoaJuridicaRequired.bind(this.tipoForm)],
            }),
            endereco: this.fb.group({
                logradouro: ['', Validators.required],
                numero: ['', Validators.required],
                complemento: [''],
                bairro: ['', Validators.required],
                cep: ['', [Validators.required, Validators.minLength(8)]],
                municipio: ['', Validators.required],
                estado: ['', Validators.required],
                pais: ['', Validators.required],
            }),
            telefone: this.fb.group(
                {
                    residencial: [
                        '',
                        [Validators.maxLength(16), Validators.minLength(8)],
                    ],
                    comercial: [
                        '',
                        [Validators.maxLength(16), Validators.minLength(8)],
                    ],
                    celular: [
                        '',
                        [Validators.maxLength(16), Validators.minLength(8)],
                    ],
                    fax: [
                        '',
                        [Validators.maxLength(16), Validators.minLength(8)],
                    ],
                },
                { validator: TelefoneValidator.validate.bind(this) }
            ),
        });
        this.pessoaForm.get('tipo').patchValue(this.tipoForm);
        this.pessoaForm.get('pessoa_juridica.substituto').patchValue('0');
        this.pessoaForm.get('endereco.pais').patchValue('1058');
        this.pessoaForm.get('tipo').valueChanges.subscribe(value => {
            this.tipoForm = value;
        });
        this.getCidades();
        this.getCep();
        this.verificarDocumento();
    }

    salvar() {
        this.validarFormulario().subscribe(res => {
            if (res) {
                this.load();
                this.salvarPessoaWs().subscribe(
                    resWs => {
                        if (resWs.success === true) {
                            this.salvarPessoaApi().subscribe(
                                result => {
                                    this.loaded();
                                    this.alerts
                                        .alertSuccess({
                                            title: 'Ok',
                                            text:
                                                'Pessoa cadastrada com sucesso',
                                        })
                                        .then();
                                },
                                error => {
                                    this.loaded();
                                    this.error(error);
                                }
                            );
                        } else {
                            this.loaded();
                            this.alerts.alertError({
                                title: 'Oops',
                                text: resWs.message,
                            });
                        }
                    },
                    error => {
                        this.loaded();
                        this.error(error);
                    }
                );
            } else {
                this.alerts.alertError({
                    title: 'Oops',
                    text: 'Formulário Inválido',
                });
            }
        });
    }

    private validarFormulario(): Observable<boolean> {
        return new Observable(observ => {
            let result: boolean = false;
            if (this.isCpf()) {
                result =
                    this.pessoaForm.get('tipo').valid &&
                    this.pessoaForm.get('nome').valid &&
                    this.pessoaForm.get('email').valid &&
                    this.pessoaForm.get('observacao').valid &&
                    this.pessoaForm.get('pis').valid &&
                    this.pessoaForm.get('pessoa_fisica.cpf').valid &&
                    this.pessoaForm.get('pessoa_fisica.rg').valid &&
                    this.pessoaForm.get('pessoa_fisica.orgao_expedidor')
                        .valid &&
                    this.pessoaForm.get('pessoa_fisica.data_expedicao').valid &&
                    this.pessoaForm.get('pessoa_fisica.data_nascimento')
                        .valid &&
                    this.pessoaForm.get('pessoa_fisica.cei').valid &&
                    this.pessoaForm.get('pessoa_fisica.cargo').valid &&
                    this.pessoaForm.get('endereco.logradouro').valid &&
                    this.pessoaForm.get('endereco.numero').valid &&
                    this.pessoaForm.get('endereco.complemento').valid &&
                    this.pessoaForm.get('endereco.bairro').valid &&
                    this.pessoaForm.get('endereco.cep').valid &&
                    this.pessoaForm.get('endereco.municipio').valid &&
                    this.pessoaForm.get('endereco.estado').valid &&
                    this.pessoaForm.get('endereco.pais').valid &&
                    !this.pessoaForm.get('telefone').hasError('telefoneNeeded');
            }

            if (this.isCnpj()) {
                result =
                    this.pessoaForm.get('tipo').valid &&
                    this.pessoaForm.get('nome').valid &&
                    this.pessoaForm.get('email').valid &&
                    this.pessoaForm.get('observacao').valid &&
                    this.pessoaForm.get('pis').valid &&
                    this.pessoaForm.get('pessoa_juridica.cnpj').valid &&
                    this.pessoaForm.get('pessoa_juridica.ie').valid &&
                    this.pessoaForm.get('pessoa_juridica.im').valid &&
                    this.pessoaForm.get('pessoa_juridica.nome_fantasia')
                        .valid &&
                    this.pessoaForm.get('pessoa_juridica.substituto').valid &&
                    this.pessoaForm.get('pessoa_juridica.website').valid &&
                    this.pessoaForm.get('pessoa_juridica.contato_nome').valid &&
                    this.pessoaForm.get('pessoa_juridica.contato_telefone')
                        .valid &&
                    this.pessoaForm.get('pessoa_juridica.contato_email')
                        .valid &&
                    this.pessoaForm.get('endereco.logradouro').valid &&
                    this.pessoaForm.get('endereco.numero').valid &&
                    this.pessoaForm.get('endereco.complemento').valid &&
                    this.pessoaForm.get('endereco.bairro').valid &&
                    this.pessoaForm.get('endereco.cep').valid &&
                    this.pessoaForm.get('endereco.municipio').valid &&
                    this.pessoaForm.get('endereco.estado').valid &&
                    this.pessoaForm.get('endereco.pais').valid &&
                    !this.pessoaForm.get('telefone').hasError('telefoneNeeded');
            }

            if (result) {
                this.setPessoaFisica();
                this.setPessoaJuridica();
                this.setEndereco();
                this.setTelefone();
            }

            observ.next(result);
        });
    }

    private salvarPessoaWs(): Observable<any> {
        return new Observable(observ => {
            const body = this.getPessoaWs();
            this.api.postWs(body).subscribe(
                res => {
                    observ.next(res);
                },
                error => {
                    observ.error(error);
                }
            );
        });
    }

    private salvarPessoaApi(): Observable<any> {
        return new Observable(observ => {
            this.salvarPessoa().subscribe(
                res => {
                    observ.next(res);
                },
                error => {
                    observ.error(error);
                }
            );
        });
    }

    private salvarTelefone(): Observable<number> {
        return new Observable(observ => {
            const body = this.telefone;
            this.api.post('telefone', body).subscribe(
                res => {
                    const id = Number(res.id);
                    observ.next(id);
                },
                error => {
                    observ.error(error);
                }
            );
        });
    }

    private salvarEndereco(): Observable<number> {
        return new Observable(observ => {
            const body = this.endereco;
            this.api.post('endereco', body).subscribe(
                res => {
                    const id = Number(res.id);
                    observ.next(id);
                },
                error => {
                    observ.error(error);
                }
            );
        });
    }

    private salvarPessoaJuridica(): Observable<number> {
        return new Observable(observ => {
            const body = this.pessoaJuridica;
            this.api.post('pessoa-juridica', body).subscribe(
                res => {
                    const id = Number(res.id);
                    observ.next(id);
                },
                error => {
                    observ.error(error);
                }
            );
        });
    }

    private salvarPessoaFisica(): Observable<number> {
        return new Observable(observ => {
            const body = this.pessoaFisica;
            this.api.post('pessoa-fisica', body).subscribe(
                res => {
                    const id = Number(res.id);
                    observ.next(id);
                },
                error => {
                    observ.error(error);
                }
            );
        });
    }

    private salvarPessoa(): Observable<any> {
        return new Observable(observ => {
            this.salvarEndereco().subscribe(enderecoId => {
                this.salvarTelefone().subscribe(
                    telefoneId => {
                        if (this.isCpf()) {
                            this.salvarPessoaFisica().subscribe(
                                pessoaFisicaId => {
                                    this.setPessoa(
                                        enderecoId,
                                        telefoneId,
                                        pessoaFisicaId
                                    );
                                    const body = this.pessoa;
                                    this.api.post('pessoa', body).subscribe(
                                        res => {
                                            observ.next(res);
                                        },
                                        error => {
                                            this.removeEndereco(enderecoId);
                                            this.removeTelefone(telefoneId);
                                            this.removePessoa(pessoaFisicaId);
                                            observ.error(error);
                                        }
                                    );
                                },
                                error => {
                                    this.removeEndereco(enderecoId);
                                    this.removeTelefone(telefoneId);
                                    observ.error(error);
                                }
                            );
                        }
                        if (this.isCnpj()) {
                            this.salvarPessoaJuridica().subscribe(
                                pessoaJuridicaId => {
                                    this.setPessoa(
                                        enderecoId,
                                        telefoneId,
                                        pessoaJuridicaId
                                    );
                                    const body = this.pessoa;
                                    this.api.post('pessoa', body).subscribe(
                                        res => {
                                            observ.next(res);
                                        },
                                        error => {
                                            this.removeEndereco(enderecoId);
                                            this.removeTelefone(telefoneId);
                                            this.removePessoa(pessoaJuridicaId);
                                            observ.error(error);
                                        }
                                    );
                                },
                                error => {
                                    this.removeEndereco(enderecoId);
                                    this.removeTelefone(telefoneId);
                                    observ.error(error);
                                }
                            );
                        }
                    },
                    error => {
                        this.removeEndereco(enderecoId);
                        observ.error(error);
                    }
                );
            });
        });
    }

    private removeTelefone(id): Observable<any> {
        return new Observable(observ => {
            this.api.delete('telefone', id).subscribe(
                res => {
                    observ.next(res);
                },
                error => {
                    console.error(error);
                }
            );
        });
    }

    private removeEndereco(id): Observable<any> {
        return new Observable(observ => {
            this.api.delete('endereco', id).subscribe(
                res => {
                    observ.next(res);
                },
                error => {
                    console.error(error);
                }
            );
        });
    }

    private removePessoa(id): Observable<any> {
        return new Observable(observ => {
            const url = this.isCnpj() ? 'pessoa-juridica' : 'pessoa-fisica';
            this.api.delete(url, id).subscribe(
                res => {
                    observ.next(res);
                },
                error => {
                    console.error(error);
                }
            );
        });
    }

    public isCpf(): boolean {
        return '' + this.pessoaForm.get('tipo').value === '1';
    }

    public isCnpj(): boolean {
        return '' + this.pessoaForm.get('tipo').value === '2';
    }

    private getPessoaWs(): URLSearchParams {
        const pessoa: URLSearchParams = new URLSearchParams();
        pessoa.set('acao', 'cadastrarPessoa');
        pessoa.set('pessoa[tipo]', this.pessoaForm.get('tipo').value);
        pessoa.set('pessoa[nome]', this.pessoaForm.get('nome').value);
        pessoa.set('pessoa[email]', this.pessoaForm.get('email').value);
        pessoa.set(
            'pessoa[observacao]',
            this.pessoaForm.get('observacao').value
        );
        pessoa.set('pessoa[pis]', this.pessoaForm.get('pis').value);
        if (this.isCpf()) {
            pessoa.set(
                'pessoa[fisica][cpf]',
                this.pessoaForm.get('pessoa_fisica.cpf').value
            );
            pessoa.set(
                'pessoa[fisica][rg]',
                this.pessoaForm.get('pessoa_fisica.rg').value
            );
            pessoa.set(
                'pessoa[fisica][orgao_expedidor]',
                this.pessoaForm.get('pessoa_fisica.orgao_expedidor').value
            );
            pessoa.set(
                'pessoa[fisica][data_expedicao]',
                this.pessoaForm.get('pessoa_fisica.data_expedicao').value
            );
            pessoa.set(
                'pessoa[fisica][data_nascimento]',
                this.pessoaForm.get('pessoa_fisica.data_nascimento').value
            );
            pessoa.set(
                'pessoa[fisica][cei]',
                this.pessoaForm.get('pessoa_fisica.cei').value
            );
            pessoa.set(
                'pessoa[fisica][cargo]',
                this.pessoaForm.get('pessoa_fisica.cargo').value
            );
        }
        if (this.isCnpj()) {
            pessoa.set(
                'pessoa[juridica][cnpj]',
                this.pessoaForm.get('pessoa_juridica.cnpj').value
            );
            pessoa.set(
                'pessoa[juridica][ie]',
                this.pessoaForm.get('pessoa_juridica.ie').value
            );
            pessoa.set(
                'pessoa[juridica][im]',
                this.pessoaForm.get('pessoa_juridica.im').value
            );
            pessoa.set(
                'pessoa[juridica][nome_fantasia]',
                this.pessoaForm.get('pessoa_juridica.nome_fantasia').value
            );
            pessoa.set(
                'pessoa[juridica][substituto]',
                this.pessoaForm.get('pessoa_juridica.substituto').value
            );
            pessoa.set(
                'pessoa[juridica][website]',
                this.pessoaForm.get('pessoa_juridica.website').value
            );
            pessoa.set(
                'pessoa[juridica][contato_nome]',
                this.pessoaForm.get('pessoa_juridica.contato_nome').value
            );
            pessoa.set(
                'pessoa[juridica][contato_telefone]',
                this.pessoaForm.get('pessoa_juridica.contato_telefone').value
            );
            pessoa.set(
                'pessoa[juridica][contato_email]',
                this.pessoaForm.get('pessoa_juridica.contato_email').value
            );
        }
        pessoa.set(
            'pessoa[endereco][logradouro]',
            this.pessoaForm.get('endereco.logradouro').value
        );
        pessoa.set(
            'pessoa[endereco][numero]',
            this.pessoaForm.get('endereco.numero').value
        );
        pessoa.set(
            'pessoa[endereco][complemento]',
            this.pessoaForm.get('endereco.complemento').value
        );
        pessoa.set(
            'pessoa[endereco][bairro]',
            this.pessoaForm.get('endereco.bairro').value
        );
        pessoa.set(
            'pessoa[endereco][cep]',
            this.pessoaForm.get('endereco.cep').value
        );
        pessoa.set(
            'pessoa[endereco][municipio]',
            this.pessoaForm.get('endereco.municipio').value
        );
        pessoa.set(
            'pessoa[endereco][estado]',
            this.pessoaForm.get('endereco.estado').value
        );
        pessoa.set(
            'pessoa[endereco][pais]',
            this.pessoaForm.get('endereco.pais').value
        );
        pessoa.set(
            'telefone[residencial]',
            this.pessoaForm.get('telefone.residencial').value
        );
        pessoa.set(
            'telefone[comercial]',
            this.pessoaForm.get('telefone.comercial').value
        );
        pessoa.set(
            'telefone[celular]',
            this.pessoaForm.get('telefone.celular').value
        );
        pessoa.set('telefone[fax]', this.pessoaForm.get('telefone.fax').value);

        return pessoa;
    }

    private getCidades() {
        this.pessoaForm
            .get('endereco.estado')
            .valueChanges.subscribe(estadoUf => {
                const body = new URLSearchParams();
                body.set('acao', 'buscarIBGE');
                body.set('uf', estadoUf);
                body.set('pais', this.pessoaForm.get('endereco.pais').value);
                this.api.postWs(body).subscribe(res => {
                    this.cidades = [{}];
                    if (res.success === true)
                        Object.assign(this.cidades, res.cidades);
                });
            });
    }

    private getCep() {
        this.pessoaForm.get('endereco.cep').valueChanges.subscribe(cep => {
            if (cep.length === 8) {
                const body = new URLSearchParams();
                body.set('acao', 'buscarCEP');
                body.set('cep', cep);
                body.set('pais', this.pessoaForm.get('endereco.pais').value);
                this.api.postWs(body).subscribe(res => {
                    if (res.success === true) {
                        if (
                            this.pessoaForm.get('endereco.logradouro').value ===
                            ''
                        )
                            this.pessoaForm
                                .get('endereco.logradouro')
                                .patchValue(res.logradouro);
                        if (this.pessoaForm.get('endereco.bairro').value === '')
                            this.pessoaForm
                                .get('endereco.bairro')
                                .patchValue(res.bairro);
                        this.pessoaForm
                            .get('endereco.estado')
                            .patchValue(res.uf);
                        this.pessoaForm
                            .get('endereco.pais')
                            .patchValue(res.codigo_pais);
                        this.pessoaForm
                            .get('endereco.municipio')
                            .patchValue(res.codigo_municipio);
                    }
                });
            }
        });
    }

    private setTelefone() {
        const telefone = {
            residencial: this.pessoaForm.get('telefone.residencial').value,
            comercial: this.pessoaForm.get('telefone.comercial').value,
            celular: this.pessoaForm.get('telefone.celular').value,
            fax: this.pessoaForm.get('telefone.fax').value,
        };
        Object.assign(this.telefone, telefone);
    }

    private setEndereco() {
        const endereco = {
            logradouro: '' + this.pessoaForm.get('endereco.logradouro').value,
            numero: '' + this.pessoaForm.get('endereco.numero').value,
            complemento: '' + this.pessoaForm.get('endereco.complemento').value,
            bairro: '' + this.pessoaForm.get('endereco.bairro').value,
            cep: '' + this.pessoaForm.get('endereco.cep').value,
            municipio: '' + this.pessoaForm.get('endereco.municipio').value,
            estado: '' + this.pessoaForm.get('endereco.estado').value,
            pais: '' + this.pessoaForm.get('endereco.pais').value,
        };
        Object.assign(this.endereco, endereco);
    }

    private setPessoaJuridica() {
        const pessoaJuridica = {
            cnpj: this.pessoaForm.get('pessoa_juridica.cnpj').value,
            ie: this.pessoaForm.get('pessoa_juridica.ie').value,
            im: this.pessoaForm.get('pessoa_juridica.im').value,
            nome_fantasia: this.pessoaForm.get('pessoa_juridica.nome_fantasia')
                .value,
            substituto: this.pessoaForm.get('pessoa_juridica.substituto').value,
            website: this.pessoaForm.get('pessoa_juridica.website').value,
            contato_nome: this.pessoaForm.get('pessoa_juridica.contato_nome')
                .value,
            contato_telefone: this.pessoaForm.get(
                'pessoa_juridica.contato_telefone'
            ).value,
            contato_email: this.pessoaForm.get('pessoa_juridica.contato_email')
                .value,
        };
        Object.assign(this.pessoaJuridica, pessoaJuridica);
    }

    private setPessoaFisica() {
        const pessoaFisica = {
            cpf: this.pessoaForm.get('pessoa_fisica.cpf').value,
            rg: this.pessoaForm.get('pessoa_fisica.rg').value,
            orgao_expedidor: this.pessoaForm.get(
                'pessoa_fisica.orgao_expedidor'
            ).value,
            data_expedicao: this.pessoaForm.get('pessoa_fisica.data_expedicao')
                .value,
            data_nascimento: this.pessoaForm.get(
                'pessoa_fisica.data_nascimento'
            ).value,
            cei: this.pessoaForm.get('pessoa_fisica.cei').value,
            cargo: this.pessoaForm.get('pessoa_fisica.cargo').value,
        };
        Object.assign(this.pessoaFisica, pessoaFisica);
    }

    private setPessoa(endereco: number, telefone: number, pessoa: number) {
        const pessoaObject = {
            tipo: this.pessoaForm.get('tipo').value,
            nome: this.pessoaForm.get('nome').value,
            email: this.pessoaForm.get('email').value,
            observacao: this.pessoaForm.get('observacao').value,
            pis: this.pessoaForm.get('pis').value,
            endereco: endereco,
            telefone: telefone,
            pessoa_fisica: this.isCpf() ? pessoa : null,
            pessoa_juridica: this.isCnpj() ? pessoa : null,
        };
        Object.assign(this.pessoa, pessoaObject);
    }

    public checkErrorOf(campo: string): {} | false {
        let valid: { campo: string; text: string } | false = false;
        this.entityErrors.forEach(value => {
            if (value.campo === campo) valid = value;
        });
        return valid;
    }

    private verificarDocumento() {
        this.pessoaForm
            .get('pessoa_fisica.cpf')
            .valueChanges.subscribe(value => {
                if (this.pessoaForm.get('pessoa_fisica.cpf').valid) {
                    this.pessoaForm.get('pessoa_fisica.rg').patchValue('');
                    this.pessoaForm
                        .get('pessoa_fisica.orgao_expedidor')
                        .patchValue('');
                    this.pessoaForm
                        .get('pessoa_fisica.data_expedicao')
                        .patchValue('');
                    this.pessoaForm
                        .get('pessoa_fisica.data_nascimento')
                        .patchValue('');
                    this.pessoaForm.get('pessoa_fisica.cei').patchValue('');
                    this.pessoaForm.get('pessoa_fisica.cargo').patchValue('');
                    this.pessoaForm.get('nome').patchValue('');
                    this.pessoaForm.get('email').patchValue('');
                    this.pessoaForm.get('endereco.logradouro').patchValue('');
                    this.pessoaForm.get('endereco.numero').patchValue('');
                    this.pessoaForm.get('endereco.complemento').patchValue('');
                    this.pessoaForm.get('endereco.bairro').patchValue('');
                    this.pessoaForm.get('endereco.cep').patchValue('');
                    this.pessoaForm.get('endereco.municipio').patchValue('');
                    this.pessoaForm.get('endereco.estado').patchValue('');
                }
            });
        this.pessoaForm
            .get('pessoa_fisica.data_nascimento')
            .valueChanges.subscribe(value => {
                if (
                    this.pessoaForm.get('pessoa_fisica.cpf').valid &&
                    this.pessoaForm.get('pessoa_fisica.cpf').value !== '' &&
                    this.pessoaForm.get('pessoa_fisica.data_nascimento')
                        .valid &&
                    this.pessoaForm.get('pessoa_fisica.data_nascimento')
                        .value !== '' &&
                    this.pessoaForm.get('pessoa_fisica.data_nascimento').value
                        .length > 8
                ) {
                    this.buscaDocumento();
                }
            });

        this.pessoaForm
            .get('pessoa_juridica.cnpj')
            .valueChanges.subscribe(() => {
                if (
                    this.pessoaForm.get('pessoa_juridica.cnpj').valid &&
                    this.pessoaForm.get('pessoa_juridica.cnpj').value !== ''
                ) {
                    this.buscaDocumento();
                }
            });
    }

    private buscaDocumento() {
        const body = new URLSearchParams();
        body.set('acao', 'verificarDocumento');
        if (this.isCpf()) {
            body.set(
                'documento',
                this.pessoaForm.get('pessoa_fisica.cpf').value
            );
            body.set('data_nascimento', this.dataNascimentoValid);
        }
        if (this.isCnpj()) {
            body.set(
                'documento',
                this.pessoaForm.get('pessoa_juridica.cnpj').value
            );
            body.set('data_nascimento', '0000-00-00');
        }

        this.api
            .postWs(body)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(res => {
                if (res.status || res.success) {
                    this.pessoaForm.get('email').patchValue(res.email);
                    this.pessoaForm.get('nome').patchValue(res.nome);
                    if (res['PF'] !== undefined && res['PF']) {
                        this.pessoaForm
                            .get('pessoa_fisica.orgao_expedidor')
                            .patchValue(res['PF'].orgao_expedidor);
                        this.pessoaForm
                            .get('pessoa_fisica.data_expedicao')
                            .patchValue(res['PF'].data_expedicao);
                        this.pessoaForm
                            .get('pessoa_fisica.cei')
                            .patchValue(res['PF'].cei);
                        this.pessoaForm
                            .get('pessoa_fisica.cargo')
                            .patchValue(res['PF'].cargo);
                    }
                    if (res['PJ'] !== undefined && res['PJ']) {
                        this.pessoaForm
                            .get('pessoa_juridica.ie')
                            .patchValue(res['PJ']['inscricao_estadual']);
                        this.pessoaForm
                            .get('pessoa_juridica.im')
                            .patchValue(res['PJ']['inscricao_municipal']);
                        this.pessoaForm
                            .get('pessoa_juridica.nome_fantasia')
                            .patchValue(res['PJ']['nome_fantasia']);
                        this.pessoaForm
                            .get('pessoa_juridica.website')
                            .patchValue(res['PJ']['website']);
                    }
                    if (res.endereco !== undefined && res.endereco) {
                        this.pessoaForm
                            .get('endereco.logradouro')
                            .patchValue(res['endereco'].logradouro);
                        this.pessoaForm
                            .get('endereco.numero')
                            .patchValue(res['endereco'].numero);
                        this.pessoaForm
                            .get('endereco.complemento')
                            .patchValue(res['endereco'].complemento);
                        this.pessoaForm
                            .get('endereco.bairro')
                            .patchValue(res['endereco'].bairro);
                        this.pessoaForm
                            .get('endereco.cep')
                            .patchValue(res['endereco'].cep);
                        this.pessoaForm
                            .get('endereco.municipio')
                            .patchValue(res['endereco']['codigo_municipio']);
                        this.pessoaForm
                            .get('endereco.estado')
                            .patchValue(res['endereco'].uf);
                    }
                }
            });
    }

    private get dataNascimentoValid(): string {
        let data = this.pessoaForm.get('pessoa_fisica.data_nascimento').value;
        const pattern = /(\d{2})\/(\d{2})\/(\d{4})/;
        data = data.replace(pattern, '$3-$2-$1');
        return `${data}`;
    }
}
