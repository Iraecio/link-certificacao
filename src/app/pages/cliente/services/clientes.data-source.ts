import { map } from 'rxjs/operators';
import { ServerDataSource } from '../../../@shared/services/server.data-source';

export class ClientesDataSource extends ServerDataSource {
    getElements(): Promise<any> {
        return this.requestElements()
            .pipe(
                map(res => {
                    this.lastRequestCount = this.extractTotalFromResponse(res);
                    this.data = this.extractDataFromResponse(res);
                    const dataTable = [];
                    this.data.forEach((value, i) => {
                        localStorage.setItem(
                            'clienteView' + value['id'],
                            JSON.stringify(value)
                        );
                        dataTable.push({
                            id: value['id'] || '',
                            nome: value['nome'] || '',
                            email: value['email'] || '',
                            cpf:
                                value && value.pessoa_fisica
                                    ? value.pessoa_fisica.cpf
                                    : value && value.pessoa_juridica
                                    ? value.pessoa_juridica.cnpj
                                    : '',
                        });
                    });
                    return dataTable;
                })
            )
            .toPromise();
    }
}
