import { Component, Injector, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseResourceComponent } from '../../../@shared/components/base-resource.component';
import { takeUntil } from 'rxjs/operators';

@Component({
    selector: 'ngx-cliente-view',
    templateUrl: './cliente-view.component.html',
})
export class ClientesViewComponent extends BaseResourceComponent<any>
    implements OnInit {
    estados = [
        { nome: 'Acre', sigla: 'AC' },
        { nome: 'Alagoas', sigla: 'AL' },
        { nome: 'Amapá', sigla: 'AP' },
        { nome: 'Amazonas', sigla: 'AM' },
        { nome: 'Bahia', sigla: 'BA' },
        { nome: 'Ceará', sigla: 'CE' },
        { nome: 'Distrito Federal', sigla: 'DF' },
        { nome: 'Espírito Santo', sigla: 'ES' },
        { nome: 'Goiás', sigla: 'GO' },
        { nome: 'Maranhão', sigla: 'MA' },
        { nome: 'Mato Grosso', sigla: 'MT' },
        { nome: 'Mato Grosso do Sul', sigla: 'MS' },
        { nome: 'Minas Gerais', sigla: 'MG' },
        { nome: 'Pará', sigla: 'PA' },
        { nome: 'Paraíba', sigla: 'PB' },
        { nome: 'Paraná', sigla: 'PR' },
        { nome: 'Pernambuco', sigla: 'PE' },
        { nome: 'Piauí', sigla: 'PI' },
        { nome: 'Rio de Janeiro', sigla: 'RJ' },
        { nome: 'Rio Grande do Norte', sigla: 'RN' },
        { nome: 'Rio Grande do Sul', sigla: 'RS' },
        { nome: 'Rondônia', sigla: 'RO' },
        { nome: 'Roraima', sigla: 'RR' },
        { nome: 'Santa Catarina', sigla: 'SC' },
        { nome: 'São Paulo', sigla: 'SP' },
        { nome: 'Sergipe', sigla: 'SE' },
        { nome: 'Tocantins', sigla: 'TO' },
    ];
    cliente: any;
    cidade: string;
    estado: string;

    constructor(protected injector: Injector) {
        super(injector);
        this.router.paramMap
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                res =>
                    (this.cliente =
                        JSON.parse(
                            localStorage.getItem('clienteView' + res.get('id'))
                        ) || {})
            );
    }

    protected get router(): ActivatedRoute {
        return this.injector.get(ActivatedRoute);
    }

    ngOnInit() {
        this.getCep(this.cliente.endereco.cep);
        console.log(this.cliente);
    }

    private getCep(cep) {
        if (cep.length === 8) {
            const body = new URLSearchParams();
            body.set('acao', 'buscarCEP');
            body.set('cep', cep);
            body.set('pais', this.cliente.endereco.pais);
            this.api.postWs(body).subscribe(res => {
                if (res.success === true) {
                    if (
                        this.cliente.endereco.municipio === res.codigo_municipio
                    ) {
                        this.cidade = res.municipio;
                    }
                    this.estados.forEach(uf => {
                        if (uf.sigla === this.cliente.endereco.estado)
                            this.estado = uf.nome;
                    });
                }
            });
        }
    }
}
