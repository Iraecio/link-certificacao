import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { ClienteComponent } from './cliente.component';
import { ClientesComponent } from './clientes/clientes.component';
import { ClientesAddComponent } from './cliente-add/cliente-add.component';
import { ClientesViewComponent } from './cliente-view/cliente-view.component';

const routes: Routes = [
    {
        path: '',
        component: ClienteComponent,
        children: [
            { path: 'clientes', component: ClientesComponent },
            { path: 'add', component: ClientesAddComponent },
            { path: ':id/view', component: ClientesViewComponent },
            { path: '', redirectTo: 'clientes', pathMatch: 'full' },
        ],
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class ClienteRoutingModule {}
