import { Component, Injector } from '@angular/core';
import { BaseResourceComponent } from '../@shared/components/base-resource.component';

import { NbMenuItem } from '@nebular/theme';

@Component({
    selector: 'ngx-pages',
    template: `
        <ngx-sample-layout>
            <nb-menu [items]="menu"></nb-menu>
            <router-outlet></router-outlet>
        </ngx-sample-layout>
    `,
})
export class PagesComponent extends BaseResourceComponent<any> {
    menu: NbMenuItem[] = [
        {
            title: 'Home',
            icon: 'nb-home',
            link: '/sistema/dashboard',
            home: true,
        },
        {
            title: 'Pedidos',
            icon: 'nb-checkmark-circle',
            link: '/sistema/pedidos',
        },
        {
            title: 'Clientes',
            icon: 'nb-person',
            link: '/sistema/clientes',
        },
        {
            title: 'Usuários',
            icon: 'nb-person',
            link: '/sistema/user/usuarios',
            hidden: this.user.level !== '1',
            expanded: false,
            children: [
                {
                    title: 'Listar Usuarios',
                    icon: 'nb-person',
                    link: '/sistema/user/usuarios',
                },
                {
                    title: 'Regiões',
                    icon: 'nb-person',
                    link: '/sistema/regiao/regiaos',
                },
            ],
        },
        {
            title: 'Boletos',
            icon: 'nb-compose',
            link: '/sistema/boleto/boletos',
        },
    ];
    constructor(protected injector: Injector) {
        super(injector);
    }
}
