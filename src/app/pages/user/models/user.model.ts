export class User {
    id: number;
    nome_razao: string;
    email: string;
    created_at: string;
    updated_at: string;
    cpf: string;
    login: string;
    status: number;
    alterar_senha: number;
    comissao: number;
    level: string;
    group: number;
    regiao: number;
}

export class Funcao {
    '1': string;
    '2': string;
    '3': string;
}

export class Status {
    '0': string;
    '1': string;
}
