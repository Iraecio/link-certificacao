import {map} from 'rxjs/operators';
import {ServerDataSource} from '../../../@shared/services/server.data-source';

export class UsuariosDataSource extends ServerDataSource {

    getElements(): Promise<any> {
        return this.requestElements().pipe(map(res => {
            this.lastRequestCount = this.extractTotalFromResponse(res);
            this.data = this.extractDataFromResponse(res);
            const user = JSON.parse(localStorage.getItem('user'));
            const dataTable = [];
            this.data.forEach((users, i) => {
                if (users.id !== user.id) {
                    dataTable.push({
                        id: users['id'] || '',
                        nome_razao: users['nome_razao'] || '',
                        cpf: users['cpf'] || '',
                        email: users['email'] || '',
                        group: users['group']['nome'] || '',
                        status: this.status(users['status']),
                    });
                }
            });

            return dataTable;
        })).toPromise();
    }

    private status(status: string = '') {
        const statu = {'1': 'Sim', '0': 'Não'};
        return status !== '' ? statu[status] : '';
    }
}
