import {Component, Injector, OnInit} from '@angular/core';
import {User} from '../../models/user.model';
import {FormGroup, Validators} from '@angular/forms';
import {cpfCnpjValidator} from '../../../../@core/validators/cpf-cnpj.validator';
import {BaseResourceComponent} from '../../../../@shared/components/base-resource.component';
import {takeUntil} from 'rxjs/operators';

@Component({
    selector: 'ngx-usuarios-add',
    templateUrl: './usuarios-add.component.html',
    styleUrls: ['./usuarios-add.component.scss']
})
export class UsuariosAddComponent extends BaseResourceComponent<any> implements OnInit {

    user: User;
    perfilForm: FormGroup;

    grupos: Array<any> = [];
    regiaos: Array<any> = [];

    formInValid: boolean;
    private usuario: {
        alterar_senha: any;
        nome_razao: any;
        password: any;
        comissao: any;
        level: any;
        cpf: any;
        regiao: any;
        login: any;
        email: any;
        status: any;
        group: any
    };

    constructor(protected injector: Injector) {
        super(injector);
        this.formInValid = false;
    }

    ngOnInit() {
        this.perfilForm = this.fb.group({
            nome_razao: ['', Validators.required],
            email: ['', [Validators.required, Validators.email]],
            cpf: ['', [Validators.required, cpfCnpjValidator()]],
            login: ['', Validators.required],
            status: ['', Validators.required],
            alterar_senha: ['', Validators.required],
            comissao: ['', Validators.required],
            level: ['', Validators.required],
            password: ['', Validators.required],
            group: ['', Validators.required],
            regiao: ['', Validators.required]
        });

        this.perfilForm.controls.status.patchValue('1');
        this.perfilForm.controls.alterar_senha.patchValue('1');
        this.perfilForm.controls.comissao.patchValue('0');

        this.api.get('usuarios?get_all=true').pipe(takeUntil(this.ngUnsubscribe)).subscribe((res) => this.onChanges(res));

        this.perfilForm.get('group').valueChanges.pipe(takeUntil(this.ngUnsubscribe)).subscribe((res) => {
            this.perfilForm.get('level').patchValue(res);
        });

        this.api.get('groups?get_all=true').pipe(takeUntil(this.ngUnsubscribe)).subscribe((res) => this.grupos = res);
        this.api.get('regiaos?get_all=true').pipe(takeUntil(this.ngUnsubscribe)).subscribe((res) => this.regiaos = res);
    }

    salvar() {
        if (this.perfilForm.valid) {
            this.setUsuario();
            this.api.post('usuarios', this.usuario)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (res) => {
                    if (res.id)
                        this.alerts.alertSuccess({title: 'OK', text: 'Usuario adicionado com sucesso'}).then(() => {
                            this.redirectView(res.id);
                        });
                }, () => {
                    this.alerts.alertError({title: 'Oops', text: 'Error no servidor'}).then();
                }
            );
        }
    }

    onChanges(result) {
        this.perfilForm.get('cpf').valueChanges.pipe(takeUntil(this.ngUnsubscribe)).subscribe(value => {
            this.formInValid = false;
            this.perfilForm.get('login').patchValue(value);
            result.forEach((user) => {
                if (user.cpf === value)
                    this.formInValid = true;
            });
        });
    }

    private setUsuario(): void {
        this.usuario = {
            nome_razao: this.perfilForm.get('nome_razao').value,
            email: this.perfilForm.get('email').value,
            cpf: this.perfilForm.get('cpf').value,
            login: this.perfilForm.get('login').value,
            status: this.perfilForm.get('status').value,
            alterar_senha: this.perfilForm.get('alterar_senha').value,
            comissao: this.perfilForm.get('comissao').value,
            level: this.perfilForm.get('level').value,
            password: this.perfilForm.get('password').value,
            group: this.perfilForm.get('group').value,
            regiao: this.perfilForm.get('regiao').value,
        };
    }

    private redirectView(id): void {
        this._route.navigateByUrl('/sistema/user/usuarios', {skipLocationChange: true}).then(() => {
            this._route.navigate(['/sistema/user/' + id + '/edit']).then();
        });
    }

}
