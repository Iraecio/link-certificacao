import {Component, Injector, OnInit} from '@angular/core';
import {FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {BaseResourceComponent} from '../../../../@shared/components/base-resource.component';
import {takeUntil} from 'rxjs/operators';

@Component({
    selector: 'ngx-usuarios-edit',
    templateUrl: './usuarios-edit.component.html',
    styleUrls: ['./usuarios-edit.component.scss']
})
export class UsuariosEditComponent extends BaseResourceComponent<any> implements OnInit {

    perfilForm: FormGroup;

    grupos: Array<any> = [];
    GrupoSelecionado: string = '';
    regiaos: Array<any> = [];
    RegiaoSelecionado: string = '';

    private usuario: {
        alterar_senha: any;
        nome_razao: any;
        password: any;
        comissao: any;
        level: any;
        cpf: any;
        regiao: any;
        login: any;
        email: any;
        status: any;
        group: any
    };

    private id: string = '0';

    constructor(protected injector: Injector) {
        super(injector);
        this.router.paramMap.pipe(takeUntil(this.ngUnsubscribe)).subscribe((res) => this.id = res.get('id'));
        this.getGrupos();
        this.getRegiaos();
    }

    protected get router(): ActivatedRoute {
        return this.injector.get(ActivatedRoute);
    }

    ngOnInit() {
        this.load();
        this.perfilForm = this.fb.group({
            nome_razao: ['', Validators.required],
            email: [{value: '', disabled: true}, [Validators.required, Validators.email]],
            cpf: [{value: '', disabled: true}],
            login: [{value: '', disabled: true}],
            status: ['', Validators.required],
            alterar_senha: ['', Validators.required],
            comissao: ['', Validators.required],
            level: ['', Validators.required],
            password: ['', Validators.required],
            group: ['', Validators.required],
            regiao: ['', Validators.required]
        });

        this.api.get('usuarios', this.id).pipe(takeUntil(this.ngUnsubscribe)).subscribe((res) => {
            this.perfilForm.patchValue(res);
            this.perfilForm.get('group').patchValue(res.group.id);
            this.perfilForm.get('regiao').patchValue(res.regiao.id);
            this.perfilForm.get('status').patchValue('' + res.status);
            this.perfilForm.get('alterar_senha').patchValue('' + res.alterar_senha);
            this.GrupoSelecionado = res.group.nome;
            this.RegiaoSelecionado = res.regiao.nome;
            this.loaded();
        }, () => {
            this.loaded();
        });

        this.perfilForm.get('group').valueChanges.pipe(takeUntil(this.ngUnsubscribe)).subscribe((res) => {
            this.perfilForm.get('level').patchValue(res);
        });
    }

    salvar() {
        if (this.perfilForm.valid) {
            this.setUsuario();
            this.api.update('usuarios', this.id, this.usuario)
                .pipe(takeUntil(this.ngUnsubscribe))
                .subscribe(
                    (res) => {
                        if (res.id) {
                            this.alerts.alertSuccess({title: 'OK', text: 'Usuario editado com sucesso'})
                                .then(() => this.redirectView(res.id));
                        }
                    }, () => {
                        this.alerts.alertError({title: 'Oops', text: 'Error no servidor'}).then();
                    }
                );
        }
    }

    private getGrupos(): void {
        this.api.get('groups?get_all=true')
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe((g) => this.grupos = g);
    }

    private getRegiaos(): void {
        this.api.get('regiaos?get_all=true')
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe((r) => this.regiaos = r);
    }

    private setUsuario(): void {
        this.usuario = {
            nome_razao: this.perfilForm.get('nome_razao').value,
            email: this.perfilForm.get('email').value,
            cpf: this.perfilForm.get('cpf').value,
            login: this.perfilForm.get('login').value,
            status: this.perfilForm.get('status').value,
            alterar_senha: this.perfilForm.get('alterar_senha').value,
            comissao: this.perfilForm.get('comissao').value,
            level: this.perfilForm.get('level').value,
            password: this.perfilForm.get('password').value,
            group: this.perfilForm.get('group').value,
            regiao: this.perfilForm.get('regiao').value,
        };
    }

    private redirectView(id): void {
        this._route.navigateByUrl('/sistema/user/usuarios', {skipLocationChange: true}).then(() => {
            this._route.navigate(['/sistema/user/' + id + '/edit']).then();
        });
    }

}
