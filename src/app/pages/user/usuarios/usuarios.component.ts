import {Component, Injector, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {BaseResourceComponent} from '../../../@shared/components/base-resource.component';
import {ServerDataSource} from '../../../@shared/services/server.data-source';
import {ServerSourceConf} from '../../../@shared/models/server-data-source.model';
import {UsuariosDataSource} from '../services/usuarios.data-source';
import {takeUntil} from 'rxjs/operators';

@Component({
    selector: 'ngx-usuarios',
    templateUrl: './usuarios.component.html',
    styleUrls: ['./usuarios.component.scss']
})
export class UsuariosComponent extends BaseResourceComponent<any> implements OnInit {
    settings = {
        add: {
            addButtonContent: '<i class="nb-plus"></i>',
            createButtonContent: '<i class="nb-checkmark"></i>',
            cancelButtonContent: '<i class="nb-close"></i>',
        },
        edit: {
            editButtonContent: '<i class="nb-edit"></i>',
            saveButtonContent: '<i class="nb-checkmark"></i>',
            cancelButtonContent: '<i class="nb-close"></i>',
        },
        delete: {
            deleteButtonContent: '<i class="nb-trash"></i>',
            confirmDelete: true,
        },
        mode: 'external',
        columns: {
            nome_razao: {
                title: 'NOME/RAZÃO',
                type: 'string',
            },
            cpf: {
                title: 'CPF/CNPJ',
                type: 'string',
            },
            email: {
                title: 'E-MAIL',
                type: 'string',
            },
            group: {
                title: 'GRUPO',
                type: 'string',
            },
            status: {
                title: 'ATIVO',
                type: 'string',
            },
        },
        sort: true,
        sortDirection: 'desc',
        pager: {
            perPage: 10
        },
        actions: {
            columnTitle: 'AÇÕES',
            position: 'right',
            add: false,
            edit: false,
            delete: false
        }
    };

    tableConf: ServerSourceConf = {
        sortFieldExcludeKey: [],
        endPoint: 'usuarios',
        includeFieldKey: 'group,regiao'
    };

    source: ServerDataSource;

    constructor(protected injector: Injector) {
        super(injector);
        this.source = new UsuariosDataSource(injector, this.tableConf);
    }

    onDeleteConfirm(event): Observable<any> {
        return new Observable(observ => {
            const user = event.data.nome_razao;
            const id = event.data.id;
            if (window.confirm('Tem certeza de que deseja excluir usuario `' + user + '`?')) {
                this.api.delete('usuarios', id)
                    .pipe(takeUntil(this.ngUnsubscribe))
                    .subscribe((res) => {
                        observ.next(res);
                    }, (error) => {
                        observ.error(error);
                    });
            }
        });
    }

    onDelete($event) {
        this.onDeleteConfirm($event)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(() => {
                this.source.remove($event.data).then();
                this.alerts.alertSuccess({title: 'Ok', text: 'Usuario removido com sucesso'}).then();
            });
    }

    onCreate(event) {
        this._route.navigate(['sistema/user/add']).then();
    }

    onEdit(event) {
        console.log(event);
        this._route.navigate(['sistema/user/' + event.data.id + '/edit']).then();
    }

    ngOnInit() {
        this.user = JSON.parse(localStorage.getItem('user'));
        const admin = ('' + this.user.level === '1');
        Object.assign(this.settings, {
            actions: {
                columnTitle: 'AÇÕES',
                position: 'right',
                add: admin,
                edit: admin,
                delete: admin,
            }
        });
    }

}
