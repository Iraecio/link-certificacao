import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {UserComponent} from './user.component';
import {PerfilComponent} from './perfil/perfil.component';
import {UsuariosComponent} from './usuarios/usuarios.component';
import {UsuariosEditComponent} from './usuarios/usuarios-edit/usuarios-edit.component';
import {UsuariosAddComponent} from './usuarios/usuarios-add/usuarios-add.component';

const routes: Routes = [{
    path: '',
    component: UserComponent,
    children: [
        {path: 'perfil', component: PerfilComponent},
        {path: 'usuarios', component: UsuariosComponent},
        {path: ':id/edit', component: UsuariosEditComponent},
        {path: 'add', component: UsuariosAddComponent},
        {path: '', redirectTo: 'perfil', pathMatch: 'full'},
    ],
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class UserRoutingModule {
}

