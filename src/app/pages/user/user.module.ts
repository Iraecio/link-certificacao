import {NgModule} from '@angular/core';
import {UserComponent} from './user.component';
import {PerfilComponent} from './perfil/perfil.component';
import {ThemeModule} from '../../@theme/theme.module';
import {UserRoutingModule} from './user-routing.module';
import {ReactiveFormsModule} from '@angular/forms';
import {UsuariosComponent} from './usuarios/usuarios.component';
import {Ng2SmartTableModule} from 'ng2-smart-table';
import {UsuariosAddComponent} from './usuarios/usuarios-add/usuarios-add.component';
import {UsuariosEditComponent} from './usuarios/usuarios-edit/usuarios-edit.component';
import {SharedModule} from '../../@shared/shared.module';

const TABLE_COMPONENT = [];

@NgModule({
    declarations: [
        UserComponent,
        PerfilComponent,
        UsuariosComponent,
        UsuariosAddComponent,
        UsuariosEditComponent,
        ...TABLE_COMPONENT,
    ],
    entryComponents: [
        ...TABLE_COMPONENT,
    ],
    imports: [
        ThemeModule,
        SharedModule,
        UserRoutingModule,
        ReactiveFormsModule,
        Ng2SmartTableModule,
    ],
})
export class UserModule {
}
