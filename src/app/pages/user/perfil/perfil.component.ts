import {ChangeDetectionStrategy, Component, Injector, OnInit} from '@angular/core';
import {FormGroup, Validators} from '@angular/forms';
import {PasswordValidator} from '../../../@core/validators/password.validator';
import {BaseResourceComponent} from '../../../@shared/components/base-resource.component';
import {takeUntil} from 'rxjs/operators';

@Component({
    selector: 'ngx-perfil',
    templateUrl: './perfil.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    styleUrls: ['./perfil.component.scss']
})
export class PerfilComponent extends BaseResourceComponent<any> implements OnInit {
    form: FormGroup;

    grupos: any;
    regiaos: any;

    grupo: string = '';
    regiao: string = '';

    flipped: boolean = false;

    constructor(protected injector: Injector) {
        super(injector);
    }

    ngOnInit() {
        this.form = this.fb.group({
            nome_razao: ['', Validators.required],
            email: ['', Validators.required],
            cpf: [{value: '', disabled: true}, Validators.required],
            login: [{value: '', disabled: true}, Validators.required],
            status: [{value: '', disabled: true}, Validators.required],
            group: [{value: '', disabled: true}, Validators.required],
            regiao: [{value: '', disabled: true}, Validators.required],
            alterar_senha: [{value: '', disabled: true}, Validators.required],
            comissao: [{value: '', disabled: true}, Validators.required],
            level: [{value: '', disabled: true}, Validators.required],
            password: ['', Validators.minLength(8)],
            password_confirm: ['']
        }, {
            validator: PasswordValidator.validate.bind(this)
        });

        this.setUser();

        this.getGrupos();
        this.getRegiaos();
    }

    toggleCard() {
        this.flipped = !this.flipped;
    }

    salvar() {
        this.api.update('usuarios', this.user.id, this.getForm())
        .pipe(takeUntil(this.ngUnsubscribe))
        .subscribe(
            (res) => {
                this.api.setItem('user', JSON.stringify(res));
                this.alerts.alertSuccess({title: 'Ok', text: 'Usuario editado com sucesso'}).then(() => {
                    this._route.navigate(['/sistema/user/usuarios'], {skipLocationChange: true}).then(() => {
                        this._route.navigate(['/sistema/user/perfil'], {skipLocationChange: true}).then();
                    });
                });
            },
            (error) => this.error(error),
        );
    }

    getForm() {
        const form = {
            nome_razao: this.form.get('nome_razao').value,
            email: this.form.get('email').value,
            cpf: this.form.get('cpf').value,
            login: this.form.get('login').value,
            status: this.form.get('status').value,
            group: this.form.get('group').value,
            regiao: this.form.get('regiao').value,
            alterar_senha: this.form.get('alterar_senha').value,
            comissao: this.form.get('comissao').value,
            level: this.form.get('level').value
        };

        if (this.form.get('password').value !== '')
            Object.assign(form, {password: this.form.get('password').value});

        return form;
    }

    private getGrupos() {
        this.api.get('groups').pipe(takeUntil(this.ngUnsubscribe))
        .subscribe((res) => this.grupos = res);
    }

    private getRegiaos() {
        this.api.get('regiaos').pipe(takeUntil(this.ngUnsubscribe))
        .subscribe((res) => this.regiaos = res);
    }

    private setUser() {
        this.regiao = this.user.regiao.nome;
        this.grupo = this.user.group.nome;
        this.form.patchValue(this.user);
        this.form.get('regiao').patchValue(this.user.regiao.id);
        this.form.get('group').patchValue(this.user.group.id);
    }
}
