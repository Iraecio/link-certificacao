import {NgModule} from '@angular/core';
import {ThemeModule} from '../../@theme/theme.module';
import {MiscellaneousRoutingModule, routedComponents} from './miscellaneous-routing.module';
import {SharedModule} from '../../@shared/shared.module';

@NgModule({
    imports: [
        ThemeModule,
        SharedModule,
        MiscellaneousRoutingModule,
    ],
    declarations: [
        ...routedComponents,
    ],
})
export class MiscellaneousModule {
}
