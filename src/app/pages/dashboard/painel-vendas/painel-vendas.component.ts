import { Component, OnDestroy } from '@angular/core';
import { takeWhile } from 'rxjs/operators';
import { OrdersChart, OrderChartSummary, OrdersChartService } from '../services/orders-chart.service';

@Component({
    selector: 'ngx-painel-vendas',
    styleUrls: ['./painel-vendas.component.scss'],
    template: `
        <nb-card size="large">
            <nb-tabset fullWidth>
                <nb-tab tabTitle="Vendas">
                    <div class="chart-container">
                        <ngx-painel-vendas-summary [summary]="chartPanelSummary"></ngx-painel-vendas-summary>
                        <ngx-painel-vendas-header [type]="period" (periodChange)="setPeriodAndGetChartData($event)"> </ngx-painel-vendas-header>
                        <ngx-grafico-vendas [ordersChartData]="ordersChartData"></ngx-grafico-vendas>
                    </div>
                </nb-tab>
            </nb-tabset>
        </nb-card>
    `,
})
export class PainelVendasComponent implements OnDestroy {
    private alive = true;

    chartPanelSummary: OrderChartSummary[];
    period: string = 'semana';
    ordersChartData: OrdersChart;

    constructor(private ordersChartService: OrdersChartService) {
        this.ordersChartService
            .getOrderChartSummary()
            .pipe(takeWhile(() => this.alive))
            .subscribe(summary => (this.chartPanelSummary = summary));
        this.getOrdersChartData(this.period);
    }

    setPeriodAndGetChartData(value: string): void {
        if (this.period !== value) {
            this.period = value;
        }

        this.getOrdersChartData(value);
    }

    getOrdersChartData(period: string) {
        this.ordersChartService
            .getOrdersChartData2(period)
            .pipe(takeWhile(() => this.alive))
            .subscribe(ordersChartData => {
                this.ordersChartData = ordersChartData;
            });
    }

    ngOnDestroy() {
        this.alive = false;
    }
}
