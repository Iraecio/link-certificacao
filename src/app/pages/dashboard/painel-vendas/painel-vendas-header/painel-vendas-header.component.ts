import { Component, EventEmitter, Input, OnDestroy, Output } from '@angular/core';
import { NbMediaBreakpoint, NbMediaBreakpointsService, NbThemeService } from '@nebular/theme';
import { takeWhile } from 'rxjs/operators';

@Component({
    selector: 'ngx-painel-vendas-header',
    styleUrls: ['./painel-vendas-header.component.scss'],
    template: `
        <div class="chart-header">
            <ngx-legend-chart [legendItems]="chartLegend"></ngx-legend-chart>
            <div class="dropdown" [ngClass]="{ 'ghost-dropdown': currentTheme === 'corporate' }" ngbDropdown>
                <button
                    type="button"
                    ngbDropdownToggle
                    class="btn"
                    [ngClass]="{
                        'btn-outline-success': currentTheme === 'default',
                        'btn-primary': currentTheme !== 'default',
                        'btn-sm': breakpoint.width <= breakpoints.is
                    }"
                >
                    {{ type }}
                </button>
                <ul class="dropdown-menu" ngbDropdownMenu>
                    <li class="dropdown-item" *ngFor="let period of types" (click)="changePeriod(period)">
                        {{ period }}
                    </li>
                </ul>
            </div>
        </div>
    `,
})
export class PainelVendasHeaderComponent implements OnDestroy {
    private alive = true;

    @Output() periodChange = new EventEmitter<string>();

    @Input() type: string = 'semana';

    types: string[] = ['semana', 'mes', 'ano'];
    chartLegend: { iconColor: string; title: string }[];
    breakpoint: NbMediaBreakpoint = { name: '', width: 0 };
    breakpoints: any;
    currentTheme: string;

    constructor(private themeService: NbThemeService, private breakpointService: NbMediaBreakpointsService) {
        this.themeService
            .getJsTheme()
            .pipe(takeWhile(() => this.alive))
            .subscribe(theme => {
                const orderProfitLegend = theme.variables.orderProfitLegend;
                this.currentTheme = theme.name;
                this.setLegendItems(orderProfitLegend);
            });

        this.breakpoints = this.breakpointService.getBreakpointsMap();
        this.themeService
            .onMediaQueryChange()
            .pipe(takeWhile(() => this.alive))
            .subscribe(([oldValue, newValue]) => {
                this.breakpoint = newValue;
            });
    }

    setLegendItems(orderProfitLegend) {
        this.chartLegend = [
            {
                iconColor: orderProfitLegend.firstItem,
                title: 'Pagos',
            },
            {
                iconColor: orderProfitLegend.secondItem,
                title: 'Cancelado',
            },
            {
                iconColor: orderProfitLegend.thirdItem,
                title: 'Todas as vendas',
            },
        ];
    }

    changePeriod(period: string): void {
        this.type = period;
        this.periodChange.emit(period);
    }

    ngOnDestroy() {
        this.alive = false;
    }
}
