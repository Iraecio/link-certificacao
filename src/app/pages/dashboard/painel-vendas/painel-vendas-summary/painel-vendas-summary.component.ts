import { Component, Input } from '@angular/core';
import { OrderChartSummary } from '../../services/orders-chart.service';

@Component({
    selector: 'ngx-painel-vendas-summary',
    styleUrls: ['./painel-vendas-summary.component.scss'],
    template: `
        <div class="summary-container">
            <div class="summory" *ngFor="let item of summary">
                <div class="title">{{ item.title }}</div>
                <div class="value">{{ item.value }}</div>
            </div>
        </div>
    `,
})
export class PainelVendasSummaryComponent {
    @Input() summary: OrderChartSummary[];
}
