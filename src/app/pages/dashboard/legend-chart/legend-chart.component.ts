import { Component, Input } from '@angular/core';

export enum NgxLegendItemColor {
    GREEN = 'green',
    PURPLE = 'purple',
    LIGHT_PURPLE = 'light-purple',
    BLUE = 'blue',
    YELLOW = 'yellow',
}

@Component({
    selector: 'ngx-legend-chart',
    styleUrls: ['./legend-chart.component.scss'],
    template: `
        <div class="legends">
            <div *ngFor="let legend of legendItems" class="legend">
                <div class="legend-item-color" [style.background]="legend.iconColor"></div>
                <div class="legend-title">{{ legend.title }}</div>
            </div>
        </div>
    `,
})
export class LegendChartComponent {
    /**
     * Take an array of legend items
     * Available iconColor: 'green', 'purple', 'light-purple', 'blue', 'yellow'
     * @type {{iconColor: string; title: string}[]}
     */
    @Input()
    legendItems: { iconColor: NgxLegendItemColor; title: string }[] = [];
}
