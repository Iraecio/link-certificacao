import { Component } from '@angular/core';

@Component({
    selector: 'ngx-dashboard',
    template: `
        <div class="row">
            <div class="col-sm-12">
                <ngx-painel-vendas></ngx-painel-vendas>
            </div>
        </div>
    `,
})
export class DashboardComponent {}
