import { NgModule } from '@angular/core';
import { NgxEchartsModule } from 'ngx-echarts';
import { NgxChartsModule } from '@swimlane/ngx-charts';

import { ThemeModule } from '../../@theme/theme.module';
import { DashboardComponent } from './dashboard.component';
import { PainelVendasComponent } from './painel-vendas/painel-vendas.component';
import { GraficoVendasComponent } from './painel-vendas/grafico-vendas/grafico-vendas.component';
import { PainelVendasHeaderComponent } from './painel-vendas/painel-vendas-header/painel-vendas-header.component';
import { PainelVendasSummaryComponent } from './painel-vendas/painel-vendas-summary/painel-vendas-summary.component';
import { LegendChartComponent } from './legend-chart/legend-chart.component';
import { SharedModule } from '../../@shared/shared.module';
import { DashboardRoutingModule } from './dashboard-routing.module';

@NgModule({
    imports: [
        ThemeModule,
        NgxEchartsModule,
        NgxChartsModule,
        DashboardRoutingModule,
        SharedModule,
    ],
    declarations: [
        DashboardComponent,
        PainelVendasComponent,
        PainelVendasHeaderComponent,
        PainelVendasSummaryComponent,
        LegendChartComponent,
        GraficoVendasComponent,
    ],
})
export class DashboardModule {}
