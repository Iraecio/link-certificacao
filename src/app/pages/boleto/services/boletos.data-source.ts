import {map} from 'rxjs/operators';
import {ServerDataSource} from '../../../@shared/services/server.data-source';
import {formatDate} from '@angular/common';
import {HttpParams} from '@angular/common/http';

export class BoletosDataSource extends ServerDataSource {

    getElements(): Promise<any> {
        return this.requestElements().pipe(map(res => {
            this.lastRequestCount = this.extractTotalFromResponse(res);
            this.data = this.extractDataFromResponse(res);
            this.data.forEach((value, index) => {
                this.data[index]['created_at_iso'] = '' + formatDate(value.created_at_iso, 'dd/MM/yyyy hh:mm', 'pt-BR');
                this.data[index]['status'] = this.convertStatus(value.status);
                value.variables.forEach((valor) => {
                    if (valor.variable === 'payer.name')
                        this.data[index]['cliente_name'] = valor.value;
                });
            });
            return this.data;
        })).toPromise();
    }

    protected addSortRequestParams(httpParams: HttpParams): HttpParams {
        if (this.sortConf) {
            this.sortConf.forEach((fieldConf) => {
                if (!(this.conf.sortFieldExcludeKey.indexOf(fieldConf.field) > -1)) {
                    if (fieldConf.field === 'created_at_iso')
                        fieldConf.field = 'created_at';

                    httpParams = httpParams.set(this.conf.sortFieldKey, fieldConf.field);
                    httpParams = httpParams.set(this.conf.sortDirKey, fieldConf.direction.toUpperCase());
                }
            });
        }

        return httpParams;
    }

    private convertStatus(status): string {
        const statuBoleto = {
            'pending': 'Pendente',
            'paid': 'Paga',
            'canceled': 'Cancelada',
            'draft': 'Rascunho',
            'partially_paid': 'Parcialmente paga',
            'refunded': 'Reembolsada',
            'expired': 'Expirada',
            'in_protest': 'Em protesto',
            'chargeback': 'Contestada',
            'in_analysis': 'Em análise'
        };
        return status !== '' ? statuBoleto[status] : '';
    }
}
