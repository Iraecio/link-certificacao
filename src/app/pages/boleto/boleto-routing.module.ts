import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {BoletoComponent} from './boleto.component';
import {BoletosDetailComponent} from './boletos-detail/boletos-detail.component';
import {BoletosComponent} from './boletos/boletos.component';

const routes: Routes = [{
    path: '',
    component: BoletoComponent,
    children: [
        {path: 'boletos', component: BoletosComponent},
        {path: ':id/edit', component: BoletosDetailComponent},
        {path: 'add', component: BoletosDetailComponent},
        {path: '', redirectTo: 'boletos', pathMatch: 'full'},
    ],
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class BoletoRoutingModule {
}

