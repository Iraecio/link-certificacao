import {NgModule} from '@angular/core';
import {BoletosComponent} from './boletos/boletos.component';
import {BoletosDetailComponent} from './boletos-detail/boletos-detail.component';
import {BoletoRoutingModule} from './boleto-routing.module';
import {BoletoComponent} from './boleto.component';
import {ThemeModule} from '../../@theme/theme.module';
import {SharedModule} from '../../@shared/shared.module';
import {ReactiveFormsModule} from '@angular/forms';
import {Ng2SmartTableModule} from 'ng2-smart-table';

const BOLETOCOMPONENTS = [
    BoletoComponent,
    BoletosComponent,
    BoletosDetailComponent
];

@NgModule({
    declarations: [
        ...BOLETOCOMPONENTS
    ],
    imports: [
        ThemeModule,
        SharedModule,
        BoletoRoutingModule,
        ReactiveFormsModule,
        Ng2SmartTableModule
    ]
})
export class BoletoModule {
}
