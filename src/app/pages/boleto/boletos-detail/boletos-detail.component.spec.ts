import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BoletosDetailComponent } from './boletos-detail.component';

describe('BoletosDetailComponent', () => {
  let component: BoletosDetailComponent;
  let fixture: ComponentFixture<BoletosDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoletosDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoletosDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
