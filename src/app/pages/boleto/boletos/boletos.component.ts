import {Component, Injector, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {BaseResourceComponent} from '../../../@shared/components/base-resource.component';
import {takeUntil} from 'rxjs/operators';
import {ServerDataSource} from '../../../@shared/services/server.data-source';
import {ServerSourceConf} from '../../../@shared/models/server-data-source.model';
import {BoletosDataSource} from '../services/boletos.data-source';

@Component({
    selector: 'ngx-boletos',
    templateUrl: './boletos.component.html',
    styleUrls: ['./boletos.component.scss']
})
export class BoletosComponent extends BaseResourceComponent<any> implements OnInit {
    title = 'Boletos';
    settings = {
        add: {
            addButtonContent: '<i class="nb-plus"></i>',
            createButtonContent: '<i class="nb-checkmark"></i>',
            cancelButtonContent: '<i class="nb-close"></i>',
        },
        edit: {
            editButtonContent: '<i class="nb-edit"></i>',
            saveButtonContent: '<i class="nb-checkmark"></i>',
            cancelButtonContent: '<i class="nb-close"></i>',
        },
        delete: {
            deleteButtonContent: '<i class="nb-trash"></i>',
            confirmDelete: true,
        },
        mode: 'external',
        columns: {
            created_at_iso: {
                title: 'CRIADA EM',
                type: 'string',
            },
            cliente_name: {
                title: 'CLIENTE',
                type: 'string',
            },
            total: {
                title: 'TOTAL',
                type: 'string',
            },
            status: {
                title: 'STATUS',
                type: 'string',
            },
        },
        sort: true,
        sortDirection: 'desc',
        pager: {
            perPage: 50
        },
        actions: {
            columnTitle: 'AÇÕES',
            position: 'right',
            add: false,
            edit: false,
            delete: false
        }
    };

    tableConf: ServerSourceConf = {
        endPoint: 'iugu-invoices',
        sortFieldKey: 'sort',
        sortFieldExcludeKey: ['cliente_name'],
        sortDirKey: 'order',
        pagerPageKey: 'page',
        pagerLimitKey: 'limit',
        filterFieldKey: '',
        totalKey: 'totalItems',
        dataKey: 'items'
    };

    source: ServerDataSource;

    constructor(protected injector: Injector) {
        super(injector);
        if (this.user['level'] !== '1')
            this._route.navigate(['sistema/dashboard'], {skipLocationChange: true}).then();

        this.source = new BoletosDataSource(injector, this.tableConf);
    }

    onDeleteConfirm(event): Observable<any> {
        return new Observable(observ => {
            const id = event.data.id;
            if (window.confirm('Tem certeza de que deseja cancelar boleto?')) {
                this.api.delete('iugu-invoices-cancel', id)
                    .pipe(takeUntil(this.ngUnsubscribe))
                    .subscribe((res) => observ.next(res),
                        (error) => observ.error(error));
            }
        });
    }

    onDelete(event) {
        this.onDeleteConfirm(event)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(() => {
                // this.source.remove(event.data).then();
                this.alerts.alertSuccess({title: 'Ok', text: 'Boleto cancelado com sucesso'}).then();
            });
    }

    onCreate(event) {
        this._route.navigate(['sistema/boleto/add']);
    }

    onEdit(event) {
        this._route.navigate(['sistema/boleto/' + event.data.id + '/edit']);
    }

    ngOnInit() {
        this.user = JSON.parse(localStorage.getItem('user'));
        const admin = ('' + this.user.level === '1');
        Object.assign(this.settings, {
            actions: {
                columnTitle: 'AÇÕES',
                position: 'right',
                add: false,
                edit: false,
                delete: false,
            }
        });
    }

}
