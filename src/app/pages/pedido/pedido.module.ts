import { NgModule } from '@angular/core';
import { PedidosComponent } from './pedidos/pedidos.component';
import { ThemeModule } from '../../@theme/theme.module';
import { PedidoRoutingModule } from './pedido-routing.module';
import { PedidosAddComponent } from './pedidos-add/pedidos-add.component';
import { PedidosViewComponent } from './pedidos-view/pedidos-view.component';
import { PessoaModalComponent } from './modais/pessoa-modal/pessoa-modal.component';
import { PessoaViewModalComponent } from './modais/pessoa-view-modal/pessoa-view-modal.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NbDialogModule } from '@nebular/theme';
import { NgxAlertsModule } from '@ngx-plus/ngx-alerts';
import { SharedModule } from '../../@shared/shared.module';

export const PEDIDOSCOMPONENTS = [
    PedidosComponent,
    PedidosAddComponent,
    PedidosViewComponent,
    PessoaModalComponent,
    PessoaViewModalComponent,
];
export const ENTRYCOMPONENTS = [PessoaModalComponent, PessoaViewModalComponent];
@NgModule({
    declarations: [...PEDIDOSCOMPONENTS],
    entryComponents: [...ENTRYCOMPONENTS],
    imports: [
        PedidoRoutingModule,
        ThemeModule,
        SharedModule,
        Ng2SmartTableModule,
        NbDialogModule.forChild(),
        NgxAlertsModule.forRoot(),
    ],
    providers: [],
})
export class PedidoModule {}
