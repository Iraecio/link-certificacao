export class PessoaModel {
    id: number;
    tipo: string;
    nome: string;
    email: string;
    observacao: string;
    pis: string;
    pessoa_fisica: PessoaFisicaModel;
    pessoa_juridica: PessoaJuridicaModel;
    endereco: EnderecoModel;
    telefone: TelefoneModel;
}

export class PessoaFisicaModel {
    id: number;
    cpf: string;
    rg: string;
    orgao_expedidor: string;
    data_expedicao: string;
    data_nascimento: string;
    cei: string;
    cargo: string;
}

export class PessoaJuridicaModel {
    id: number;
    cnpj: string;
    ie: string;
    im: string;
    nome_fantasia: string;
    substituto: string;
    website: string;
    contato_nome: string;
    contato_telefone: string;
    contato_email: string;
}

export class EnderecoModel {
    id: number;
    logradouro: string;
    numero: string;
    complemento: string;
    bairro: string;
    cep: string;
    municipio: string;
    estado: string;
    pais: string;
}

export class TelefoneModel {
    id: number;
    residencial: string;
    comercial: string;
    celular: string;
    fax: string;
}
