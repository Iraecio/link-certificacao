export class Pedido {
    id: number;
    status: string;
    data_emissao: string;
    valor_total: string;
}

export class ContaReceber {
    id: number;
    status: string;
    data_emissao: string;
    data_vencimento: string;
    valor_total: string;
    valor_recebido: string | null;
    linha_digital: string;
}

export class Cliente {
    cpf_cnpj: string;
    nome: string;
}

export class ResponsavelLegal {
    cpf_cnpj: string;
    nome: string;
}

export class EmissaoNota {
    cpf_cnpj: string;
    nome: string;
    logradouro: string;
    numero: string;
    complemento: string | null;
    bairro: string;
    cep: string;
    municipio: string;
    uf: string;
    codigo_ibge: string;
}

export class Protocolos {
    protocolo: string;
    status: string;
    data_emissao: string | null;
    data_vencimento: string | null;
    avp_cpf: string | null;
    avp_nome: string | null;
}

export class Produtos {
    id: number;
    nome: string;
    valor_unitario: string;
    imposto: string;
    desconto: string;
}
