import { Component, Injector, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseResourceComponent } from '../../../@shared/components/base-resource.component';
import { ServerDataSource } from '../../../@shared/services/server.data-source';
import { ServerSourceConf } from '../../../@shared/models/server-data-source.model';
import { PedidosDataSource } from '../services/pedidos.data-source';

@Component({
    selector: 'ngx-pedidos',
    templateUrl: './pedidos.component.html',
    styleUrls: ['./pedidos.component.scss'],
})
export class PedidosComponent extends BaseResourceComponent<any>
    implements OnInit {
    settings = {
        noDataMessage: 'Nenhum pedido',
        add: {
            addButtonContent: '<i class="nb-plus"></i>',
            createButtonContent: '<i class="nb-checkmark"></i>',
            cancelButtonContent: '<i class="nb-close"></i>',
        },
        edit: {
            editButtonContent: '<i class="nb-edit"></i>',
            saveButtonContent: '<i class="nb-checkmark"></i>',
            cancelButtonContent: '<i class="nb-close"></i>',
        },
        delete: {
            deleteButtonContent: '<i class="nb-trash"></i>',
            confirmDelete: true,
        },
        mode: 'external',
        columns: {
            pedidoId: {
                title: 'ID',
                type: 'string',
            },
            cliente: {
                title: 'CLIENTE',
                type: 'string',
            },
            cpf: {
                title: 'CPF/CNPJ',
                type: 'string',
            },
            fatura: {
                title: 'FATURA',
                type: 'string',
            },
            vendedor: {
                title: 'VENDEDOR',
                type: 'string',
            },
        },
        sort: true,
        sortDirection: 'desc',
        pager: {
            perPage: 10,
        },
        actions: {
            columnTitle: 'AÇÕES',
            position: 'right',
            add: true,
            edit: true,
            delete: true,
        },
    };

    tableConf: ServerSourceConf = {
        includeFieldKey:
            'cliente.pessoa_fisica,cliente.pessoa_juridica,produtos,usuario_id',
        sortFieldExcludeKey: ['vendedor', 'cpf', 'fatura'],
        endPoint: 'pedido',
    };

    sources: ServerDataSource;

    constructor(protected injector: Injector) {
        super(injector);
        this.sources = new PedidosDataSource(injector, this.tableConf);
    }

    onEdit(event) {
        this._route
            .navigate(['sistema/pedidos/' + event.data.id + '/edit'])
            .then();
    }

    onDeleteConfirm(event): Observable<any> {
        return new Observable(observ => {
            const user = event.data.cliente;
            const id = event.data.id;
            if (
                window.confirm(
                    'Tem certeza de que deseja excluir pedido `' + user + '`?'
                )
            ) {
                this.load();
                this.removerPedidoWs(id).subscribe(
                    () => {
                        this.api.delete('pedido', id).subscribe(
                            res => {
                                this.loaded();
                                observ.next(res);
                            },
                            error => {
                                this.loaded();
                                observ.error(error);
                            }
                        );
                    },
                    error => {
                        this.loaded();
                        observ.error(error);
                    }
                );
            }
        });
    }

    onDelete(event) {
        this.onDeleteConfirm(event).subscribe(() => {
            this.sources.remove(event.data).then(() => {
                this.alerts
                    .alertSuccess({
                        title: 'Ok',
                        text: 'Pedido removido com sucesso',
                    })
                    .then();
            });
        });
    }

    onCreate(event) {
        this._route.navigate(['sistema/pedidos/add']).then();
    }

    ngOnInit() {}

    private removerPedidoWs(id): Observable<any> {
        return new Observable(observ => {
            const pedidos = this.sources['data'];
            pedidos.forEach(pedido => {
                if (pedido['id'] === id) {
                    const produtos = pedido['produtos'];
                    if (undefined !== produtos && produtos.length > 0) {
                        produtos.forEach((produto, index, arr) => {
                            const body = new URLSearchParams();
                            body.set('acao', 'cancelarPedido');
                            body.set('idPedido', produto['pedido_id']);
                            this.api.postWs(body).subscribe(
                                result => {
                                    if (result.success) {
                                        if (index === arr.length - 1)
                                            observ.next(true);
                                    } else {
                                        if (index === arr.length - 1)
                                            observ.error(result);
                                    }
                                },
                                error => {
                                    observ.error(error);
                                }
                            );
                        });
                    } else {
                        observ.next(true);
                    }
                }
            });
        });
    }
}
