import { Component, Injector, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Address, CustomVariables, Fatura, Items, Payer } from '../../../@shared/models/iugu-fatura';
import { BaseResourceComponent } from '../../../@shared/components/base-resource.component';
import { takeUntil } from 'rxjs/operators';

@Component({
    selector: 'ngx-pedidos-view',
    templateUrl: './pedidos-view.component.html',
    styleUrls: ['./pedidos-view.component.scss'],
})
export class PedidosViewComponent extends BaseResourceComponent<any> implements OnInit {
    id: number;
    data: string;

    public pedido: any;
    public boletoStatus: any;
    private pedidoWs: any;
    private pedidoApi: any;

    private boletoUrl: string;
    private boletoFatura = <Fatura>{};

    constructor(protected injector: Injector) {
        super(injector);
        this.router.paramMap.pipe(takeUntil(this.ngUnsubscribe)).subscribe(rota => {
            this.id = +rota.get('id');
        });
    }

    protected get router(): ActivatedRoute {
        return this.injector.get(ActivatedRoute);
    }

    private static dataFormatada(data: Date): string {
        const dia = data.getDate().toString();
        const diaF = dia.length === 1 ? '0' + dia : dia;
        const mes = (data.getMonth() + 1).toString();
        const mesF = mes.length === 1 ? '0' + mes : mes;
        const anoF = data.getFullYear();
        return anoF + '-' + mesF + '-' + diaF;
    }

    ngOnInit() {
        this.load();
        this.getPedido().pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(resApi => {
                this.getPedidoWs().pipe(takeUntil(this.ngUnsubscribe))
                    .subscribe(resWs => {
                        this.pedidoApi = resApi;
                        this.pedidoWs = resWs['pedidos'];
                        this.pedidoApi['produtos'].forEach((produto, index) => {
                            this.pedidoWs.forEach(pedido => {
                                if (+produto['pedido_id'] === pedido['pedido']['id']) {
                                    this.pedidoApi['produtos'][index]['pedidos'] = pedido;
                                    this.pedido = this.pedidoApi;
                                    this.log(this.pedido);
                                }
                            });
                        });
                        this.loaded();
                    });
            });
    }

    imprimirBoleto() {
        this.load();
        this.gerarBoleto()
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                res => {
                    this.loaded();
                    this.alerts
                        .alertSuccess({
                            title: 'Ok',
                            text: 'Boleto gerado com sucesso',
                        })
                        .then(() => {
                            window.open(res.secure_url, '_blank');
                        });
                },
                error => {
                    this.loaded();
                    this.error(error);
                    this.alerts
                        .alertError({
                            title: 'Oops',
                            text: 'Erro ao gerar boleto',
                        })
                        .then();
                },
                () => this.loaded()
            );
    }

    onDeleteConfirm(): Observable<any> {
        return new Observable(observable => {
            if (window.confirm('Tem certeza de que deseja excluir pedido?')) {
                this.removerPedidoWs()
                    .pipe(takeUntil(this.ngUnsubscribe))
                    .subscribe(() => {
                        this.api
                            .delete('pedido', '' + this.id)
                            .pipe(takeUntil(this.ngUnsubscribe))
                            .subscribe(
                                res => {
                                    observable.next(res);
                                },
                                error => {
                                    observable.error(error);
                                }
                            );
                    });
            }
        });
    }

    cancelar() {
        this.onDeleteConfirm()
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(() => {
                this.alerts
                    .alertSuccess({
                        title: 'Ok',
                        text: 'Pedido removido com sucesso',
                    })
                    .then(() => {
                        this._route.navigate(['/sistema/pedidos']);
                    });
            });
    }

    gerarProtocolo(event, produto) {
        this.load();
        event.preventDefault();
        const body = new URLSearchParams();
        body.set('acao', 'gerarProtocolo');
        body.set('idPedido', produto['pedido_id']);
        body.set('idProduto', produto['produto']);
        this.api
            .postWs(body)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(res => {
                if (res.success === true) {
                    const data = {
                        pedido: produto['pedido'],
                        pedido_id: produto['pedido_id'],
                        produto: produto['produto'],
                        protocolo: res.protocolo,
                    };
                    this.api
                        .update('pedido-produto', produto['id'], data)
                        .pipe(takeUntil(this.ngUnsubscribe))
                        .subscribe(
                            () => {
                                this.loaded();
                                this.alerts
                                    .alertSuccess({
                                        title: 'Ok',
                                        text: res.message,
                                    })
                                    .then(() => {
                                        this._route
                                            .navigate(['/sistema/pedidos'], {
                                                skipLocationChange: true,
                                            })
                                            .then(() => {
                                                this._route.navigate(['/sistema/pedidos/' + this.id + '/edit']);
                                            });
                                    });
                            },
                            () => {
                                this.loaded();
                                this.alerts
                                    .alertError({
                                        title: 'Oops',
                                        text: 'Error no servidor',
                                    })
                                    .then();
                            }
                        );
                } else {
                    this.loaded();
                    this.alerts.alertError({ title: 'Oops', text: res.message }).then();
                }
            });
    }

    public buscarFatura() {
        if (this.pedido && this.pedido['boleto'] !== null) {
            this.api
                .get('iugu-invoices', this.pedido['boleto']['id'])
                .pipe(takeUntil(this.ngUnsubscribe))
                .subscribe(
                    res => {
                        window.open(res.secure_url, '_blank');
                    },
                    error => {
                        this.error(error);
                    }
                );
        }
    }

    public segundaViaBoleto() {
        if (this.pedido && this.pedido['boleto'] !== null) {
            const dataVencimento = new Date();
            dataVencimento.setDate(dataVencimento.getDate() + 4);
            const data = PedidosViewComponent.dataFormatada(dataVencimento);
            this.load();
            this.api
                .update('iugu-invoices', this.pedido['boleto']['id'], {
                    due_date: data,
                })
                .pipe(takeUntil(this.ngUnsubscribe))
                .subscribe(
                    res => {
                        this.alerts
                            .alertSuccess({
                                title: 'Ok',
                                text: '2ª Via do Boleto gerado com sucesso',
                            })
                            .then(() => {
                                window.open(res.secure_url, '_blank');
                            });
                    },
                    error => this.error(error),
                    () => this.loaded()
                );
        }
        return;
    }

    private gerarItemsBoleto() {
        const items = [];
        const produtos = this.pedido['produtos'];
        produtos.forEach(produto => {
            const item = <Items>{
                description: produto['pedidos']['produtos'][0]['nome'],
                price_cents: produto['pedidos']['produtos'][0]['valor_unitario'] * 100,
                quantity: 1,
            };
            items.push(item);
        });

        Object.assign(this.boletoFatura, { items: items });
    }

    private gerarTelefone(): string {
        let telefone = this.pedido['produtos'][0]['pedidos']['cliente']['telefones'];

        if (telefone['residencial'] !== null) {
            telefone = telefone['residencial'];
        } else if (telefone['celular'] !== null) {
            telefone = telefone['celular'];
        } else if (telefone['comercial'] !== null) {
            telefone = telefone['comercial'];
        } else if (telefone['fax'] !== null) {
            telefone = telefone['fax'];
        }
        return telefone;
    }

    private gerarFatura() {
        const cliente = this.pedido['produtos'][0]['pedidos']['cliente'];
        const endereco = this.pedido['produtos'][0]['pedidos']['cliente']['endereco'];
        const telefone = this.gerarTelefone();

        const dataVencimento = new Date();
        dataVencimento.setDate(dataVencimento.getDate() + 2);

        const fatura = <Fatura>{
            pedido_id: this.pedido['id'],
            email: cliente['email'],
            custom_variables: [{ name: 'vendedor#' + this.user['id'], value: this.user['id'] }],
            due_date: PedidosViewComponent.dataFormatada(dataVencimento),
            ensure_workday_due_date: true,
            payer: <Payer>{
                cpf_cnpj: cliente['cpf_cnpj'],
                name: cliente['nome'],
                email: cliente['email'],
                phone_prefix: telefone.substring(0, 2),
                phone: telefone,
                address: <Address>{
                    zip_code: endereco['cep'],
                    street: endereco['logradouro'],
                    number: endereco['numero'],
                    district: endereco['bairro'],
                    city: endereco['municipio'],
                    state: endereco['uf'],
                    country: 'Brasil',
                    complement: endereco['complemento'] !== null ? endereco['complemento'] : '',
                },
            },
        };
        Object.assign(this.boletoFatura, fatura);
    }

    private gerarBoleto(): Observable<any> {
        return new Observable(observ => {
            this.gerarFatura();
            this.gerarItemsBoleto();
            this.api
                .post('iugu-invoices', this.boletoFatura)
                .pipe(takeUntil(this.ngUnsubscribe))
                .subscribe(
                    res => {
                        this.boletoUrl = res.secure_url;
                        observ.next(res);
                    },
                    error => {
                        observ.error(error);
                    }
                );
        });
    }

    private removeProduto(id): Observable<any> {
        return new Observable(observ => {
            const arr = this.pedido['produtos'];
            this.api
                .delete('pedido-produto', id)
                .pipe(takeUntil(this.ngUnsubscribe))
                .subscribe(
                    res => {
                        arr.forEach((produto, index) => {
                            if (produto['pedido_id'] === id) this.pedido['produtos'].splice(index, 1);
                        });
                        observ.next(res);
                    },
                    error => {
                        console.error(error);
                    }
                );
        });
    }

    private removerPedidoWs(): Observable<any> {
        return new Observable(observ => {
            const produtos = this.pedido['produtos'];
            produtos.forEach((produto, index, arr) => {
                const body = new URLSearchParams();
                body.set('acao', 'cancelarPedido');
                body.set('idPedido', produto['pedido_id']);
                this.api
                    .postWs(body)
                    .pipe(takeUntil(this.ngUnsubscribe))
                    .subscribe(
                        res => {
                            this.removeProduto(produto['id']);
                            if (index === arr.length - 1) observ.next(res);
                        },
                        error => {
                            observ.error(error);
                        }
                    );
            });
        });
    }

    private getPedido(): Observable<any> {
        return new Observable(observ => {
            this.api
                .get('pedido', '' + this.id)
                .pipe(takeUntil(this.ngUnsubscribe))
                .subscribe(
                    res => {
                        this.data = res.created_at;
                        if (res && res['boleto'] !== null) this.statusBoleto(res['boleto'] !== null ? res['boleto']['boleto']['status'] : '');
                        observ.next(res);
                    },
                    error => {
                        observ.error(error);
                    }
                );
        });
    }

    private getPedidoWs(): Observable<any> {
        return new Observable(observ => {
            const dataInicio = new Date(this.data);
            const dataFim = new Date(this.data);
            dataFim.setDate(dataInicio.getDate() + 7);

            const body = new URLSearchParams();
            body.set('acao', 'buscarPedidos');
            if (dataInicio) body.set('dados[data_inicio]', PedidosViewComponent.dataFormatada(dataInicio));
            if (dataFim) body.set('dados[data_fim]', PedidosViewComponent.dataFormatada(dataFim));

            this.api
                .postWs(body)
                .pipe(takeUntil(this.ngUnsubscribe))
                .subscribe(
                    res => {
                        observ.next(res);
                    },
                    error => {
                        observ.error(error);
                    }
                );
        });
    }

    private statusBoleto(status: string) {
        const statuBoleto = {
            pending: 'Pendente',
            paid: 'Paga',
            canceled: 'Cancelada',
            draft: 'Rascunho',
            partially_paid: 'Parcialmente paga',
            refunded: 'Reembolsada',
            expired: 'Expirada',
            in_protest: 'Em protesto',
            chargeback: 'Contestada',
            in_analysis: 'Em análise',
        };
        this.boletoStatus = status !== '' ? statuBoleto[status] : '';
    }
}
