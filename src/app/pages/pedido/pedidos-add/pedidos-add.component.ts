import { Component, Injector, OnInit, OnDestroy } from '@angular/core';
import { Validators } from '@angular/forms';
import { NbDialogService } from '@nebular/theme';
import { PessoaModel } from '../models/pessoa.model';
import {
    cpfCnpjValidator,
    cpfValidator,
} from '../../../@core/validators/cpf-cnpj.validator';
import { PessoaModalComponent } from '../modais/pessoa-modal/pessoa-modal.component';
import { PessoaViewModalComponent } from '../modais/pessoa-view-modal/pessoa-view-modal.component';
import { Observable } from 'rxjs';
import { BaseResourceComponent } from '../../../@shared/components/base-resource.component';
import { takeUntil } from 'rxjs/operators';

@Component({
    selector: 'ngx-pedidos-add',
    templateUrl: './pedidos-add.component.html',
    styleUrls: ['./pedidos-add.component.scss'],
})
export class PedidosAddComponent extends BaseResourceComponent<any>
    implements OnInit, OnDestroy {
    clienteNotExists: boolean = true;
    representante_legalNotExists: boolean = true;
    nota_fiscalNotExists: boolean = true;
    intermediarioNotExists: boolean = true;

    pedidoForm = this.fb.group({
        cliente: this.fb.group({
            id: ['', Validators.required],
            cpf: ['', cpfCnpjValidator()],
            nome_razao: [''],
        }),
        representante_legal: this.fb.group({
            id: [''],
            cpf: ['', cpfValidator()],
            nome_razao: [''],
        }),
        nota_fiscal: this.fb.group({
            id: ['', Validators.required],
            cpf: ['', cpfValidator()],
            nome_razao: [''],
        }),
        intermediario: this.fb.group({
            id: [''],
            cpf: ['', cpfValidator()],
            nome_razao: [''],
        }),
        produto: [''],
    });

    produtosWs;
    produtosRegiao;
    produtosParaEscolher;
    produtosEscolhidos: Array<object> = [];
    user;

    public total: number;
    public representante_legalNeeded: boolean = false;
    public pessoas: Array<PessoaModel>;
    public produtosEnviados: any;
    public pedidoIdApi: number = null;

    constructor(protected injector: Injector) {
        super(injector);
    }

    protected get dialogService(): NbDialogService {
        return this.injector.get(NbDialogService);
    }

    ngOnDestroy() {
        if (!localStorage.getItem('tipoDeClienteParaAdd')) {
            this.clearStorage();
        }
    }

    ngOnInit() {
        this.onChanges();
        this.getProdutosWs();
        this.getProdutosRegiao();

        this.getDadosGuardados();
    }

    getDadosGuardados() {
        const cliente = localStorage.getItem('clienteCpfAdicionado');
        const representante_legal = localStorage.getItem(
            'representante_legalCpfAdicionado'
        );
        const nota_fiscal = localStorage.getItem('nota_fiscalCpfAdicionado');
        const intermediario = localStorage.getItem(
            'intermediarioCpfAdicionado'
        );

        if (cliente) {
            this.pedidoForm
                .get('cliente.cpf')
                .patchValue(localStorage.getItem('clienteCpfAdicionado'));
        }
        if (representante_legal) {
            this.pedidoForm
                .get('representante_legal.cpf')
                .patchValue(representante_legal);
        }
        if (nota_fiscal) {
            this.pedidoForm.get('nota_fiscal.cpf').patchValue(nota_fiscal);
        }
        if (intermediario) {
            this.pedidoForm.get('intermediario.cpf').patchValue(intermediario);
        }
    }

    private clearStorage() {
        localStorage.removeItem('clienteCpfAdicionado');
        localStorage.removeItem('representante_legalCpfAdicionado');
        localStorage.removeItem('nota_fiscalCpfAdicionado');
        localStorage.removeItem('intermediarioCpfAdicionado');
        localStorage.removeItem('tipoDeClienteParaAdd');
        localStorage.removeItem('pedidoCpf');
    }

    getTotal(): number {
        this.total = Number(0);
        this.produtosEscolhidos.forEach(produto => {
            this.total = this.total + parseFloat(produto['total_venda']);
        });
        return this.total;
    }

    adicionarProduto() {
        if (this.pedidoForm.get('produto').value) {
            const id = Number('' + this.pedidoForm.get('produto').value);
            this.produtosParaEscolher.forEach(produto => {
                if (produto.id === id) {
                    this.produtosEscolhidos.push(produto);
                }
            });
        }
        this.pedidoForm.get('produto').patchValue('');
    }

    removeProdutoAdicionado(event, produto) {
        event.preventDefault();
        const index: number = this.produtosEscolhidos.indexOf(produto);
        if (index !== -1) this.produtosEscolhidos.splice(index, 1);
    }

    enviarPedido() {
        this.load();
        if (this.validarForm()) {
            this.realizarPedido()
                .pipe(takeUntil(this.ngUnsubscribe))
                .subscribe(
                    res => {
                        this.loaded();
                        if (res.success === true)
                            this.alerts
                                .alertSuccess({
                                    title: 'Ok',
                                    text: '' + res.message,
                                })
                                .then(() => {
                                    this.clearStorage();
                                    this._route
                                        .navigate(['/sistema/pedidos'])
                                        .then();
                                });
                    },
                    error => {
                        this.loaded();
                        this.alerts
                            .alertError({
                                title: 'Oops',
                                text: `${error.message}`,
                            })
                            .then();
                    }
                );
        } else {
            this.loaded();
            this.alerts.alertError({
                title: 'Oops',
                text: 'Formulario inválido',
            });
        }
    }

    viewClient(tipo) {
        this.dialogService.open(PessoaViewModalComponent, {
            context: { id: 1 },
        });
    }

    adicionarPessoa(tipo): void {
        const form = this.pedidoForm.get(tipo + '.cpf');
        if (
            (form.value.length > 0 && form.errors) ||
            form.hasError('cpfNotValid') ||
            form.hasError('cnpjNotValid')
        ) {
            this.alerts.alertError({
                title: 'Error',
                text: 'Cpf/Cnpj Inválido',
            });
        } else if (!this[tipo + 'NotExists']) {
            this.alerts.alertInfo({
                title: 'Aviso',
                text: 'Cliente já esta cadastrado',
            });
        } else {
            localStorage.setItem('tipoDeClienteParaAdd', tipo);
            localStorage.setItem('pedidoCpf', form.value);
            this.dialogService.open(PessoaModalComponent);
        }
    }

    produtoChange(produto, total: boolean = false) {
        const i = this.produtosEscolhidos.indexOf(produto);

        if (
            +this.produtosEscolhidos[i]['desconto_venda'] >
            +this.produtosEscolhidos[i]['desconto_regiao']
        ) {
            this.produtosEscolhidos[i]['desconto_venda'] = +this
                .produtosEscolhidos[i]['desconto_regiao'];
            console.log(this.produtosEscolhidos[i]['desconto_venda']);
        }

        if (total) {
            this.produtosEscolhidos[i]['valor_venda'] = this.produtosEscolhidos[
                i
            ]['total_venda'];
            this.produtosEscolhidos[i]['desconto_venda'] = 0;
        }

        this.produtosEscolhidos[i]['total_venda'] =
            this.produtosEscolhidos[i]['valor_venda'] -
            (this.produtosEscolhidos[i]['valor_venda'] / 100) *
                this.produtosEscolhidos[i]['desconto_venda'];
    }

    private validarForm(): boolean {
        return (
            this.pedidoForm.controls.cliente.valid &&
            this.produtosEscolhidos.length > 0
        );
    }

    public clearForm(formulario): void {
        this.pedidoForm.get(formulario + '.id').patchValue('');
        this.pedidoForm.get(formulario + '.cpf').patchValue('');
        this.pedidoForm.get(formulario + '.nome_razao').patchValue('');
    }

    private getProdutosRegiao() {
        this.api
            .get('regiaos', this.user.regiao.id)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(res => {
                this.produtosRegiao = res['produtos'];
            });
    }

    public checkPessoa(value): Observable<any> {
        return new Observable(observ => {
            let query = '';
            if (value.length === 11) {
                query = '?get_cpf=' + value;
            } else if (value.length > 11) {
                query = '?get_cnpj=' + value;
            }
            if (value.length >= 11) {
                this.load();
                this.api
                    .get('pessoa' + query)
                    .pipe(takeUntil(this.ngUnsubscribe))
                    .subscribe(
                        res => {
                            if (res.id) {
                                localStorage.setItem(
                                    'clienteView' + res.id,
                                    JSON.stringify(res)
                                );
                            }
                            observ.next(res);
                        },
                        error => {
                            observ.error(error);
                        },
                        () => {
                            this.loaded();
                        }
                    );
            }
        });
    }

    private onChanges() {
        this.pedidoForm
            .get('cliente.cpf')
            .valueChanges.pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(value => {
                this.clienteNotExists = true;
                this.pedidoForm
                    .get('cliente')
                    .patchValue({ nome_razao: '', id: '' });
                this.representante_legalNeeded = false;
                this.checkPessoa(value)
                    .pipe(takeUntil(this.ngUnsubscribe))
                    .subscribe(pessoa => {
                        if (this.isCnpjOrCpf(pessoa, value)) {
                            this.clienteNotExists = false;
                            this.pedidoForm.get('cliente').patchValue({
                                nome_razao: pessoa.nome,
                                id: pessoa.id,
                            });
                            this.pedidoForm.get('nota_fiscal').patchValue({
                                nome_razao: pessoa.nome,
                                cpf: this.pedidoForm.get('cliente.cpf').value,
                                id: pessoa.id,
                            });
                            if (this.isCnpj(pessoa))
                                this.representante_legalNeeded = true;

                            this.setProdutos(pessoa);
                        }
                    });
            });

        this.pedidoForm
            .get('representante_legal.cpf')
            .valueChanges.pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(value => {
                this.representante_legalNotExists = true;
                this.pedidoForm
                    .get('representante_legal')
                    .patchValue({ nome_razao: '', id: '' });
                this.checkPessoa(value)
                    .pipe(takeUntil(this.ngUnsubscribe))
                    .subscribe((pessoa: PessoaModel) => {
                        if (this.isCnpjOrCpf(pessoa, value)) {
                            this.representante_legalNotExists = false;
                            this.pedidoForm
                                .get('representante_legal')
                                .patchValue({
                                    nome_razao: pessoa.nome,
                                    id: pessoa.id,
                                });
                        }
                    });
            });

        this.pedidoForm
            .get('nota_fiscal.cpf')
            .valueChanges.pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(value => {
                this.nota_fiscalNotExists = true;
                this.pedidoForm
                    .get('nota_fiscal')
                    .patchValue({ nome_razao: '', id: '' });
                this.checkPessoa(value)
                    .pipe(takeUntil(this.ngUnsubscribe))
                    .subscribe((pessoa: PessoaModel) => {
                        if (this.isCnpjOrCpf(pessoa, value)) {
                            this.nota_fiscalNotExists = false;
                            this.pedidoForm.get('nota_fiscal').patchValue({
                                nome_razao: pessoa.nome,
                                id: pessoa.id,
                            });
                        }
                    });
            });

        this.pedidoForm
            .get('intermediario.cpf')
            .valueChanges.pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(value => {
                this.intermediarioNotExists = true;
                this.pedidoForm
                    .get('intermediario')
                    .patchValue({ nome_razao: '', id: '' });
                this.checkPessoa(value)
                    .pipe(takeUntil(this.ngUnsubscribe))
                    .subscribe((pessoa: PessoaModel) => {
                        if (this.isCnpjOrCpf(pessoa, value)) {
                            this.intermediarioNotExists = false;
                            this.pedidoForm.get('intermediario').patchValue({
                                nome_razao: pessoa.nome,
                                id: pessoa.id,
                            });
                        }
                    });
            });
    }

    private realizarPedido(): Observable<any> {
        return new Observable(observ => {
            this.produtosEnviados = [];
            this.produtosEscolhidos.forEach(produto => {
                const body = new URLSearchParams();
                body.set('acao', 'gerarPedido');
                body.set(
                    'dados[cliente]',
                    '' + this.pedidoForm.get('cliente.cpf').value
                );
                if (this.representante_legalNeeded)
                    body.set(
                        'dados[representante_legal]',
                        '' +
                            this.pedidoForm.get('representante_legal.cpf').value
                    );
                body.set(
                    'dados[emissao_nota]',
                    '' + this.pedidoForm.get('nota_fiscal.cpf').value
                );
                body.set(
                    'dados[intermediario]',
                    '' + this.pedidoForm.get('intermediario.cpf').value
                );
                body.set('produto[codigo]', produto['id']);
                body.set('produto[valor]', produto['valor_venda']);
                body.set('produto[desconto]', produto['desconto_venda']);

                this.api
                    .postWs(body)
                    .pipe(takeUntil(this.ngUnsubscribe))
                    .subscribe(
                        resWs => {
                            if (resWs.success === true) {
                                this.produtosEnviados.push({
                                    id: resWs.id,
                                    produto: produto['id'],
                                });
                                if (
                                    this.produtosEnviados.length ===
                                    this.produtosEscolhidos.length
                                ) {
                                    this.savePedido()
                                        .pipe(takeUntil(this.ngUnsubscribe))
                                        .subscribe(resApi => {
                                            this.produtosEnviados.forEach(
                                                (value, index, arr) => {
                                                    this.saveProduto(
                                                        resApi.id,
                                                        value.id,
                                                        value.produto
                                                    )
                                                        .pipe(
                                                            takeUntil(
                                                                this
                                                                    .ngUnsubscribe
                                                            )
                                                        )
                                                        .subscribe(
                                                            res => {
                                                                if (
                                                                    index ===
                                                                    arr.length -
                                                                        1
                                                                )
                                                                    observ.next(
                                                                        {
                                                                            success: true,
                                                                            message:
                                                                                'Pedido realizado com sucesso',
                                                                        }
                                                                    );
                                                            },
                                                            error =>
                                                                observ.error(
                                                                    error
                                                                )
                                                        );
                                                }
                                            );
                                        });
                                }
                            } else {
                                observ.error(resWs);
                            }
                        },
                        error => observ.error(error)
                    );
            });

            this.produtosEnviados.forEach((value, index) => {
                console.log(value);
            });
        });
    }

    private savePedido(): Observable<any> {
        return new Observable(observ => {
            const body = {
                cliente: '' + this.pedidoForm.get('cliente.id').value,
                representante_legal: this.representante_legalNeeded
                    ? '' + this.pedidoForm.get('representante_legal.id').value
                    : null,
                emissao_nota: '' + this.pedidoForm.get('nota_fiscal.id').value,
                intermediario: this.pedidoForm.get('intermediario.id').value
                    ? '' + this.pedidoForm.get('intermediario.id').value
                    : null,
                usuario_id: this.user.id,
            };
            if (this.pedidoIdApi === null) {
                this.api
                    .post('pedido', body)
                    .pipe(takeUntil(this.ngUnsubscribe))
                    .subscribe(res => {
                        this.pedidoIdApi = res.id;
                        observ.next(res);
                    });
            } else {
                observ.next(this.pedidoIdApi);
            }
        });
    }

    private saveProduto(pedido, pedidoId, produtoId): Observable<any> {
        return new Observable(observ => {
            const body = {
                pedido: pedido,
                produto: produtoId,
                pedido_id: pedidoId,
            };
            this.api
                .post('pedido-produto', body)
                .pipe(takeUntil(this.ngUnsubscribe))
                .subscribe(res => {
                    observ.next(res);
                });
        });
    }

    private getProdutosWs() {
        const body = new URLSearchParams();
        body.set('acao', 'buscarProdutos');
        this.api
            .postWs(body)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(res => {
                this.produtosWs = res.produtos;
            });
    }

    private isCnpjOrCpf(pessoa, value): boolean {
        return (
            (pessoa.pessoa_fisica !== null &&
                pessoa.pessoa_fisica !== undefined &&
                pessoa.pessoa_fisica.cpf === value) ||
            (pessoa.pessoa_juridica !== null &&
                pessoa.pessoa_juridica !== undefined &&
                pessoa.pessoa_juridica.cnpj === value)
        );
    }

    private isCpf(pessoa: PessoaModel): boolean {
        return pessoa.tipo === '1';
    }

    private isCnpj(pessoa: PessoaModel): boolean {
        return pessoa.tipo === '2';
    }

    private setProdutos(pessoa) {
        const produtosWs = this.produtosWs;
        const produtos = [];
        if (this.isCpf(pessoa)) {
            produtosWs.forEach(res => {
                if (res.categoria === 'e-CPF') produtos.push(res);
            });
        } else {
            produtosWs.forEach(res => {
                if (res.categoria === 'e-CNPJ') produtos.push(res);
            });
        }

        const produtosRegiao = this.produtosRegiao;
        if (undefined !== produtosRegiao && produtosRegiao.length > 0) {
            produtosRegiao.forEach(produto => {
                produtos.forEach(res => {
                    if (res.id === produto.produto_id) {
                        res.valor_venda = parseFloat(
                            produto.valor_venda.replace(/,/g, '')
                        );
                        res.desconto_venda = 0;
                        res.total_venda = res.valor_venda;
                        Object.assign(res, {
                            desconto_regiao: produto.desconto_venda || 0,
                        });
                    }
                });
            });
        }
        this.produtosParaEscolher = produtos;
    }
}
