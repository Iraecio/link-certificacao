import { Component, Injector, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { BaseResourceComponent } from '../../../../@shared/components/base-resource.component';
import { NbDialogRef } from '@nebular/theme';

@Component({
    selector: 'ngx-pessoa-view-modal',
    templateUrl: './pessoa-view-modal.component.html',
    styleUrls: ['./pessoa-view-modal.component.scss'],
})
export class PessoaViewModalComponent extends BaseResourceComponent<any>
    implements OnInit, OnDestroy {
    estados = [
        { nome: 'Acre', sigla: 'AC' },
        { nome: 'Alagoas', sigla: 'AL' },
        { nome: 'Amapá', sigla: 'AP' },
        { nome: 'Amazonas', sigla: 'AM' },
        { nome: 'Bahia', sigla: 'BA' },
        { nome: 'Ceará', sigla: 'CE' },
        { nome: 'Distrito Federal', sigla: 'DF' },
        { nome: 'Espírito Santo', sigla: 'ES' },
        { nome: 'Goiás', sigla: 'GO' },
        { nome: 'Maranhão', sigla: 'MA' },
        { nome: 'Mato Grosso', sigla: 'MT' },
        { nome: 'Mato Grosso do Sul', sigla: 'MS' },
        { nome: 'Minas Gerais', sigla: 'MG' },
        { nome: 'Pará', sigla: 'PA' },
        { nome: 'Paraíba', sigla: 'PB' },
        { nome: 'Paraná', sigla: 'PR' },
        { nome: 'Pernambuco', sigla: 'PE' },
        { nome: 'Piauí', sigla: 'PI' },
        { nome: 'Rio de Janeiro', sigla: 'RJ' },
        { nome: 'Rio Grande do Norte', sigla: 'RN' },
        { nome: 'Rio Grande do Sul', sigla: 'RS' },
        { nome: 'Rondônia', sigla: 'RO' },
        { nome: 'Roraima', sigla: 'RR' },
        { nome: 'Santa Catarina', sigla: 'SC' },
        { nome: 'São Paulo', sigla: 'SP' },
        { nome: 'Sergipe', sigla: 'SE' },
        { nome: 'Tocantins', sigla: 'TO' },
    ];
    cliente: any;
    cidade: string;
    estado: string;
    id: number;
    constructor(
        protected injector: Injector,
        protected ref: NbDialogRef<PessoaViewModalComponent>
    ) {
        super(injector);
    }

    dismiss() {
        this.ref.close();
    }

    ngOnInit() {
        this.cliente = JSON.parse(
            localStorage.getItem('clienteView' + this.id)
        );
        this.getCep(this.cliente.endereco.cep);
    }

    private getCep(cep) {
        if (cep.length === 8) {
            const body = new URLSearchParams();
            body.set('acao', 'buscarCEP');
            body.set('cep', cep);
            body.set('pais', this.cliente.endereco.pais);
            this.api.postWs(body).subscribe(res => {
                if (res.success === true) {
                    if (
                        this.cliente.endereco.municipio === res.codigo_municipio
                    ) {
                        this.cidade = res.municipio;
                    }
                    this.estados.forEach(uf => {
                        if (uf.sigla === this.cliente.endereco.estado)
                            this.estado = uf.nome;
                    });
                }
            });
        }
    }
}
