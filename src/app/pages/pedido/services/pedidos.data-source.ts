import {map} from 'rxjs/operators';
import {ServerDataSource} from '../../../@shared/services/server.data-source';

export class PedidosDataSource extends ServerDataSource {

    getElements(): Promise<any> {
        return this.requestElements().pipe(map(res => {
            this.lastRequestCount = this.extractTotalFromResponse(res);
            this.data = this.extractDataFromResponse(res);

            const dataTable = [];
            this.data.forEach((value) => {
                let vendedor = value.usuario_id.nome_razao;
                vendedor = vendedor[0].toUpperCase() + vendedor.slice(1).toLowerCase();
                dataTable.push({
                    id: value.id,
                    pedidoId: value.produtos ? value.produtos[0].pedido_id : value.id ? value.id : 0,
                    cliente: value.cliente.nome,
                    fatura: PedidosDataSource.statusBoleto(value['boleto'] !== null ? value['boleto']['boleto']['status'] : ''),
                    cpf: (value.cliente && value.cliente.pessoa_fisica) ? value.cliente.pessoa_fisica.cpf :
                        (value.cliente && value.cliente.pessoa_juridica) ? value.cliente.pessoa_juridica.cnpj : '',
                    vendedor: vendedor
                });
            });

            return dataTable;
        })).toPromise();
    }

    private static statusBoleto(status: string) {
        const statuBoleto = {
            'pending': 'Pendente',
            'paid': 'Paga',
            'canceled': 'Cancelada',
            'draft': 'Rascunho',
            'partially_paid': 'Parcialmente paga',
            'refunded': 'Reembolsada',
            'expired': 'Expirada',
            'in_protest': 'Em protesto',
            'chargeback': 'Contestada',
            'in_analysis': 'Em análise'
        };
        return status !== '' ? statuBoleto[status] : 'Sem fatura';
    }

}
