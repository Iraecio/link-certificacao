import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {PedidosComponent} from './pedidos/pedidos.component';
import {PedidosAddComponent} from './pedidos-add/pedidos-add.component';
import {PedidosViewComponent} from './pedidos-view/pedidos-view.component';

const routes: Routes = [
    {path: '', component: PedidosComponent},
    {path: 'add', component: PedidosAddComponent},
    {path: ':id/edit', component: PedidosViewComponent}
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class PedidoRoutingModule {
}

