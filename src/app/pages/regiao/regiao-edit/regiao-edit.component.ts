import {Component, Injector, OnInit} from '@angular/core';
import {BaseResourceComponent} from '../../../@shared/components/base-resource.component';
import {Observable} from 'rxjs';
import {ActivatedRoute} from '@angular/router';
import {ProdutosModel, ProdutosWs, ProdutosWsModel, Regiao} from '../models/produtos.model';
import {takeUntil} from 'rxjs/operators';
import {formatNumber} from '@angular/common';

@Component({
    selector: 'ngx-regiao-edit',
    templateUrl: './regiao-edit.component.html',
    styleUrls: ['./regiao-edit.component.scss']
})
export class RegiaoEditComponent extends BaseResourceComponent<any> implements OnInit {

    public produtos: Array<ProdutosModel>;
    public regiao: Regiao = null;
    public descontoGeral: any = 0;
    protected produtosWs: Array<ProdutosWsModel>;
    protected produtosRegiao: Array<ProdutosModel>;
    private produtosParaEnviar: Array<ProdutosModel>;

    constructor(protected injector: Injector) {
        super(injector);
    }

    protected get router(): ActivatedRoute {
        return this.injector.get(ActivatedRoute);
    }

    private get id(): number {
        let id: number = null;
        this.router.paramMap.subscribe((res) => id = +res.get('id'));
        return id;
    }

    descontoChange(evento) {
        this.produtos.forEach((value) => {
            value.desconto_venda = +this.descontoGeral;
        });
    }

    ngOnInit() {
        this.load();
        this.getRegiao(this.id).pipe(takeUntil(this.ngUnsubscribe)).subscribe(
            (regiao) => {
                this.regiao = regiao;
                this.produtosRegiao = regiao.produtos;
                this.getProdutosWs().pipe(takeUntil(this.ngUnsubscribe)).subscribe(
                    (produtos: ProdutosWs) => {
                        this.produtosWs = produtos.produtos;
                        this.setProdutos();
                        this.loaded();
                    },
                    (error) => {
                        this.loaded();
                        this.error(error);
                    }
                );
            }, (error) => {
                this.loaded();
                this.error(error);
            });
    }

    changeValue(value, produtoIndex) {
        this.produtos[produtoIndex].total_venda = value;
        this.produtos[produtoIndex].valor_venda = value;
    }

    salvar() {
        this.load();
        this.produtosParaEnviar = [];
        this.produtos.forEach((produtosEditado) => {
            const produto = {
                categoria: produtosEditado.categoria,
                desconto: produtosEditado.desconto,
                desconto_venda: produtosEditado.desconto_venda,
                nome: produtosEditado.nome,
                total: produtosEditado.total,
                total_venda: formatNumber(Number(produtosEditado.total_venda), 'en', '1.2'),
                valor: produtosEditado.valor,
                valor_venda: formatNumber(Number(produtosEditado.valor_venda), 'en', '1.2'),
                produto_id: produtosEditado.id,
                regiao_id: this.regiao.id
            };
            this.produtosParaEnviar.push(produto);
        });

        this.produtosParaEnviar.forEach((produto, i, arr) => {
            this.produtosRegiao.forEach((value) => {
                if (value.produto_id === produto.produto_id)
                    Object.assign(produto, {update: value.id});
            });

            if (produto['update'] !== undefined && produto['update']) {
                const id = produto['update'];
                delete produto['update'];
                this.updateProduto(id, produto)
                .pipe(takeUntil(this.ngUnsubscribe))
                .subscribe(() => this.success(i, arr),
                    error => this.error(error));
            } else {
                this.postProduto(produto).pipe(takeUntil(this.ngUnsubscribe))
                .subscribe(() => this.success(i, arr),
                    error => this.error(error));
            }
        });
    }

    private getRegiao(id): Observable<Regiao | any> {
        return new Observable((observ) => {
            this.api.get('regiaos', id)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (regiao: Regiao) => observ.next(regiao),
                (error) => observ.error(error)
            );
        });
    }

    private getProdutosWs(): Observable<ProdutosWs | any> {
        return new Observable((observ) => {
            const body = new URLSearchParams();
            body.set('acao', 'buscarProdutos');
            this.api.postWs(body)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe((produtos: ProdutosWs) => {
                if (produtos.success) {
                    observ.next(produtos);
                } else {
                    observ.error(produtos);
                }
            });
        });
    }

    private setProdutos() {
        const produtosWs = this.produtosWs;
        const produtosRegiao = this.produtosRegiao;
        if (undefined !== produtosRegiao && produtosRegiao.length > 0) {
            produtosWs.forEach((produtoWs: ProdutosWsModel) => {
                produtosRegiao.forEach((produtosApi: ProdutosModel) => {
                    if (produtoWs.id === produtosApi.produto_id) {
                        produtoWs.valor_venda = '' + produtosApi.valor_venda;
                        produtoWs.desconto_venda = +produtosApi.desconto_venda;
                        const desconto = +produtoWs.valor_venda - ((+produtoWs.valor_venda / 100) * +produtoWs.desconto_venda);
                        produtoWs.total_venda = '' + desconto;
                    }
                });
            });
        }
        this.produtos = produtosWs;
    }

    private success(index, arr: Array<any>): void {
        if (index === arr.length - 1) {
            this.loaded();
            this.alerts.alertSuccess({title: 'Ok', text: 'Região atualizada com sucesso'}).then(() => {
                this._route.navigate(['sistema/regiao/regiaos']).then(() => {
                });
            });
        }
    }

    private updateProduto(id, produto) {
        return new Observable((observ) => {
            this.api.update('produtos', id, produto).subscribe(value => {
                observ.next(value);
            }, (error) => {
                observ.error(error);
            });
        });
    }

    private postProduto(produto) {
        return new Observable((observ) => {
            this.api.post('produtos', produto).subscribe(value => {
                observ.next(value);
            }, (error) => {
                observ.error(error);
            });
        });
    }
}
