export class ProdutosWs {
    success?: boolean;
    message?: boolean;
    produtos?: Array<ProdutosWsModel>;
}

export class ProdutosWsModel {
    categoria?: string;
    desconto?: string;
    desconto_venda?: number;
    id?: number;
    nome?: string;
    total?: string;
    total_venda?: string;
    valor?: string;
    valor_venda?: string;
}

export class ProdutosModel {
    categoria?: string;
    desconto?: string;
    desconto_venda?: number;
    id?: number;
    nome?: string;
    total?: string;
    total_venda?: string;
    valor?: string;
    valor_venda?: string;
    produto_id?: number;
    regiao_id?: number;
    created_at?: string;
    updated_at?: string;
}

export class Regiao {
    id?: any;
    nome?: any;
    produtos?: Array<ProdutosModel>;
}
