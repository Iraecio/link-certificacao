import {Component, Injector, OnInit} from '@angular/core';
import {FormGroup, Validators} from '@angular/forms';
import {BaseResourceComponent} from '../../../@shared/components/base-resource.component';
import {Observable} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
    selector: 'ngx-regiao-add',
    templateUrl: './regiao-add.component.html',
    styleUrls: ['./regiao-add.component.scss']
})
export class RegiaoAddComponent extends BaseResourceComponent<any> implements OnInit {

    form: FormGroup;

    constructor(protected injector: Injector) {
        super(injector);
    }

    ngOnInit() {
        this.form = this.fb.group({
            nome: ['', Validators.required],
        });
    }

    salvar() {
        if (this.form.valid) {
            this.enviarRegiao()
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe((res) => {
                this.alerts.alertSuccess({title: 'Ok', text: 'Regiao Adicionada com sucesso'}).then(() => {
                    this._route.navigate([`sistema/regiao/regiaos`], {skipLocationChange: true}).then(() => {
                        this._route.navigate([`sistema/regiao/${res.id}/edit`]).then();
                    });
                });
            }, (error) => {
                this.loaded();
                this.error(error);
            });
        }
    }

    private enviarRegiao(): Observable<any> {
        return new Observable((observ) => {
            this.api.post('regiaos', {nome: this.form.get('nome').value})
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe((res) => {
                observ.next(res);
            }, (error) => {
                observ.error(error);
            });
        });
    }

}
