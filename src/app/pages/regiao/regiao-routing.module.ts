import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {RegiaoComponent} from './regiao.component';
import {RegiaosComponent} from './regiaos/regiaos.component';
import {RegiaoAddComponent} from './regiao-add/regiao-add.component';
import {RegiaoEditComponent} from './regiao-edit/regiao-edit.component';

const routes: Routes = [{
    path: '',
    component: RegiaoComponent,
    children: [
        {path: 'regiaos', component: RegiaosComponent},
        {path: 'add', component: RegiaoAddComponent},
        {path: ':id/edit', component: RegiaoEditComponent},
        {path: '', redirectTo: 'regiaos', pathMatch: 'full'}
    ],
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class RegiaoRoutingModule {
}
