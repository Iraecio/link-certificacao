import {Component} from '@angular/core';

@Component({
    selector: 'ngx-regiao',
    template: '<router-outlet></router-outlet>',
})
export class RegiaoComponent {
}
