import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RegiaosComponent} from './regiaos/regiaos.component';
import {RegiaoComponent} from './regiao.component';
import {ThemeModule} from '../../@theme/theme.module';
import {SharedModule} from '../../@shared/shared.module';
import {ReactiveFormsModule} from '@angular/forms';
import {Ng2SmartTableModule} from 'ng2-smart-table';
import {RegiaoRoutingModule} from './regiao-routing.module';
import {RegiaoAddComponent} from './regiao-add/regiao-add.component';
import {RegiaoEditComponent} from './regiao-edit/regiao-edit.component';

const REGIAOCOMPONENT = [
    RegiaoComponent,
    RegiaosComponent,
    RegiaoAddComponent,
    RegiaoEditComponent
];

const ENTRYCOMPONENT = [];

@NgModule({
    declarations: [
        ...REGIAOCOMPONENT,
    ],
    entryComponents: [
        ...ENTRYCOMPONENT
    ],
    imports: [
        RegiaoRoutingModule,
        CommonModule,
        ThemeModule,
        SharedModule,
        ReactiveFormsModule,
        Ng2SmartTableModule,
    ]
})
export class RegiaoModule {
}
