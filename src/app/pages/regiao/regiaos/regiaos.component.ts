import { Component, Injector, OnInit } from '@angular/core';
import { BaseResourceComponent } from '../../../@shared/components/base-resource.component';
import { Observable } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ServerSourceConf } from '../../../@shared/models/server-data-source.model';
import { ServerDataSource } from '../../../@shared/services/server.data-source';

@Component({
    selector: 'ngx-regiaos',
    templateUrl: './regiaos.component.html',
    styleUrls: ['./regiaos.component.scss'],
})
export class RegiaosComponent extends BaseResourceComponent<any>
    implements OnInit {
    settings = {
        add: {
            addButtonContent: '<i class="nb-plus"></i>',
            createButtonContent: '<i class="nb-checkmark"></i>',
            cancelButtonContent: '<i class="nb-close"></i>',
        },
        edit: {
            editButtonContent: '<i class="nb-edit"></i>',
            saveButtonContent: '<i class="nb-checkmark"></i>',
            cancelButtonContent: '<i class="nb-close"></i>',
        },
        delete: {
            deleteButtonContent: '<i class="nb-trash"></i>',
            confirmDelete: true,
        },
        mode: 'external',
        columns: {
            nome: {
                title: 'REGIÃO',
                type: 'string',
            },
        },
        sort: true,
        sortDirection: 'desc',
        pager: {
            perPage: 10,
        },
        actions: {
            columnTitle: 'AÇÕES',
            position: 'right',
            add: false,
            edit: false,
            delete: false,
        },
    };

    tableConf: ServerSourceConf = {
        endPoint: 'regiaos',
    };

    source: ServerDataSource;

    constructor(protected injector: Injector) {
        super(injector);

        this.source = new ServerDataSource(injector, this.tableConf);
    }

    onDeleteConfirm(event): Observable<any> {
        return new Observable(observ => {
            const regiao = event.data.nome;
            const id = event.data.id;
            if (
                window.confirm(
                    'Tem certeza de que deseja excluir região `' + regiao + '`?'
                )
            ) {
                this.api
                    .delete('regiaos', id)
                    .pipe(takeUntil(this.ngUnsubscribe))
                    .subscribe(
                        res => observ.next(res),
                        error => observ.error(error)
                    );
            }
        });
    }

    onDelete(event) {
        const id = event.data.id;
        if (id === 1) {
            this.alerts
                .alertInfo({
                    title: 'Atenção',
                    text: 'A Regiao padrão nao pode ser removida',
                })
                .then();
        } else if (
            this.user &&
            this.user.level !== 1 &&
            this.user.level !== '1'
        ) {
            this.alerts
                .alertInfo({
                    title: 'Atenção',
                    text: 'Apenas Administradores podem remover uma região',
                })
                .then();
        } else {
            this.onDeleteConfirm(event)
                .pipe(takeUntil(this.ngUnsubscribe))
                .subscribe(() => {
                    this.source.remove(event.data).then();
                    this.alerts
                        .alertSuccess({
                            title: 'Ok',
                            text: 'Região removida com sucesso',
                        })
                        .then();
                });
        }
    }

    onCreate(event) {
        this._route.navigate(['/sistema/regiao/add']).then();
    }

    onEdit(event) {
        this._route
            .navigate(['/sistema/regiao/' + event.data.id + '/edit'])
            .then();
    }

    ngOnInit() {
        const admin = '' + this.user.level === '1';
        Object.assign(this.settings, {
            actions: {
                columnTitle: 'AÇÕES',
                position: 'right',
                add: admin,
                edit: admin,
                delete: admin,
            },
        });
    }
}
