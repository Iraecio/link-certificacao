import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { PagesComponent } from './pages.component';
import { NotFoundComponent } from './miscellaneous/not-found/not-found.component';

const routes: Routes = [
    {
        path: '',
        component: PagesComponent,
        children: [
            { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
            {
                path: 'dashboard',
                loadChildren: './dashboard/dashboard.module#DashboardModule',
            },
            {
                path: 'user',
                loadChildren: './user/user.module#UserModule',
            },
            {
                path: 'pedidos',
                loadChildren: './pedido/pedido.module#PedidoModule',
            },
            {
                path: 'regiao',
                loadChildren: './regiao/regiao.module#RegiaoModule',
            },
            {
                path: 'boleto',
                loadChildren: './boleto/boleto.module#BoletoModule',
            },
            {
                path: 'clientes',
                loadChildren: './cliente/cliente.module#ClienteModule',
            },
            { path: '**', component: NotFoundComponent },
        ],
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class PagesRoutingModule {}
