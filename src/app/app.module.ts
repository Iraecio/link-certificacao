import {
    APP_BASE_HREF,
    Location,
    LocationStrategy,
    PathLocationStrategy,
} from '@angular/common';
import { LOCALE_ID, NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { CoreModule } from './@core/core.module';
import { ThemeModule } from './@theme/theme.module';
import { registerLocaleData } from '@angular/common';
import localePt from '@angular/common/locales/pt';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';

registerLocaleData(localePt, 'pt');
@NgModule({
    declarations: [AppComponent],
    imports: [
        AppRoutingModule,
        CoreModule.forRoot(),
        ThemeModule.forRoot(),
        ServiceWorkerModule.register('ngsw-worker.js', {
            enabled: environment.production,
        }),
    ],
    bootstrap: [AppComponent],
    providers: [
        Location,
        { provide: APP_BASE_HREF, useValue: '/' },
        { provide: LocationStrategy, useClass: PathLocationStrategy },
        { provide: LOCALE_ID, useValue: 'pt' },
    ],
})
export class AppModule {}
